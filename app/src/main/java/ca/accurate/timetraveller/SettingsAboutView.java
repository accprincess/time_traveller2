package ca.accurate.timetraveller;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by P on 2017-01-30.
 */

public class SettingsAboutView extends FrameLayout {
    //private LayoutInflater layoutInflater;
    private ViewGroup viewGroup;
    private FragmentManager fragmentManager;
    private Context conContext;
    private View settingsAboutView;
    private SettingsViewPager settingsViewPager;


    public SettingsAboutView(Context context, ViewGroup container, LayoutInflater inflater, FragmentManager fManager, SettingsViewPager setViewPager) {
        super(context);
        settingsViewPager = setViewPager;
        conContext = context;
        viewGroup = container;
        //layoutInflater = inflater;
        fragmentManager = fManager;
        LayoutInflater layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        settingsAboutView = layoutInflater.inflate(R.layout.settings_about_view, container, false);
        ImageView imageView = (ImageView) settingsAboutView.findViewById(R.id.main_image);
        Glide.with(context).load(getResources()
                .getIdentifier("main_logo", "drawable", context.getPackageName()))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView);

        this.addView(settingsAboutView);

    }

    public void ResetData() {
        TextView setting_about_description = (TextView) settingsAboutView.findViewById(R.id.setting_about_description);
        setting_about_description.setMovementMethod(LinkMovementMethod.getInstance());
        ImageButton backBtn = (ImageButton) settingsAboutView.findViewById(R.id.back_description);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Spinnaker-Regular.ttf");
        setting_about_description.setTypeface(tf);

        if(settingsAboutView.getParent()!=null)
            ((ViewGroup)settingsAboutView.getParent()).removeView(settingsAboutView); // <- fix
        viewGroup.addView(settingsAboutView);
        backBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsViewPager.setCurrentItem(0);
            }
        });
    }

    public View GetView() {
        return settingsAboutView;
    }

}
