package ca.accurate.timetraveller;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by P on 2017-01-24.
 */

public class SingleStoryDescriptionView extends FrameLayout {
    //private LayoutInflater layoutInflater;
    private ViewGroup viewGroup;
    private FragmentManager fragmentManager;
    private Context conContext;
    private View singleStoryDescriptionView;
    private TimelineMainViewPager timelineMainViewPager;
    private MapViewPager mapViewPager;
    private NotificationViewPager notificationViewPager;
    private View sliderMediaView, singleStoryView;
    private int vidCount = CurrentStory.getInstance().getVideos().length;
    private int mediaCount = CurrentStory.getInstance().getImages().length + CurrentStory.getInstance().getVideos().length;
    private ImageButton prevBtn, nextBtn;
    private ViewPager vFlip;
    private TextView sliderDescription;
    private MainAppActivity activity;


    public SingleStoryDescriptionView(Context context, ViewGroup container, LayoutInflater inflater, FragmentManager fManager, TimelineMainViewPager tmMainVP, MapViewPager mVPager, NotificationViewPager nHViewPager) {
        super(context);
        timelineMainViewPager = tmMainVP;
        mapViewPager = mVPager;
        notificationViewPager = nHViewPager;
        conContext = context;
        viewGroup = container;
        //layoutInflater = inflater;
        fragmentManager = fManager;
        LayoutInflater layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        singleStoryDescriptionView = layoutInflater.inflate(R.layout.single_story_description, container, false);
        activity = (MainAppActivity) conContext;
        this.addView(singleStoryDescriptionView);
    }

    public void ResetData() {
        TextView storyTitle = (TextView) singleStoryDescriptionView.findViewById(R.id.storyTitle);
        TextView textDescription = (TextView) singleStoryDescriptionView.findViewById(R.id.story_description);
        ImageButton backBtn = (ImageButton) singleStoryDescriptionView.findViewById(R.id.back_description);
        prevBtn = (ImageButton) singleStoryDescriptionView.findViewById(R.id.story_desc_prev);
        nextBtn = (ImageButton) singleStoryDescriptionView.findViewById(R.id.story_desc_next);
        String year = CurrentStory.getInstance().getYear();
        String title = CurrentStory.getInstance().getTitle();

        //Media Slider
        vFlip = (ViewPager) singleStoryDescriptionView.findViewById(R.id.viewFlipper);
        SingleStoryDescriptionView.SingleStorySliderAdapter storyAdapter = new SingleStoryDescriptionView.SingleStorySliderAdapter();
        vFlip.setAdapter(storyAdapter);
        vFlip.setOffscreenPageLimit(4);
        // put year and title with different colors
        SpannableStringBuilder builder = new SpannableStringBuilder();

        // style the year
        SpannableString whiteYear= new SpannableString(year);
        whiteYear.setSpan(new ForegroundColorSpan(Color.WHITE), 0, year.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(whiteYear);

        // style the title
        SpannableString blackTitle = new SpannableString(title);
        blackTitle.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(" " + blackTitle);

        // show title with year in white
        storyTitle.setText(builder);

        // show content
        textDescription.setText(CurrentStory.getInstance().getContent());

        sliderDescription = (TextView) singleStoryDescriptionView.findViewById(R.id.slider_description);


        if(singleStoryDescriptionView.getParent()!=null)
            ((ViewGroup)singleStoryDescriptionView.getParent()).removeView(singleStoryDescriptionView); // <- fix
        viewGroup.addView(singleStoryDescriptionView);
        //container.addView(v);

        vFlip.setContentDescription("Image gallery");
        final String captions[] = CurrentStory.getInstance().getImageCaptions();

        sliderDescription.setText(captions[0]);
        prevBtn.setAlpha((float)0.5);
        prevBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItemPrev(0);
                vFlip.setCurrentItem(current);
                nextBtn.setAlpha((float)1.0);
                if(current > -1){
                    sliderDescription.setText(captions[current]);
                    prevBtn.setAlpha((float)1.0);
                    if (current == 0) {
                        prevBtn.setAlpha((float)0.5);
                    }
                }
            }
        });

        nextBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(0);
                vFlip.setCurrentItem(current);
                prevBtn.setAlpha((float)1.0);
                if(current < captions.length){
                    sliderDescription.setText(captions[current]);
                    nextBtn.setAlpha((float)1.0);
                    if (current == captions.length) {
                        nextBtn.setAlpha((float)0.5);
                    }
                }
            }
        });

        if (CurrentStory.getInstance().getImages().length <= 1) {
            prevBtn.setVisibility(INVISIBLE);
            nextBtn.setVisibility(INVISIBLE);
        }

        backBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapViewPager != null){
                    mapViewPager.setCurrentItem(CurrentStory.getInstance().STORY_VIEW);
                }
                else if(notificationViewPager != null){
                    notificationViewPager.setCurrentItem(0);
                }
                else{
                    timelineMainViewPager.setCurrentItem(CurrentStory.getInstance().STORY_VIEW);
                }
            }
        });

        vFlip.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position < captions.length){
                    sliderDescription.setText(captions[position]);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
    private int getItemPrev(int i) {
        return vFlip.getCurrentItem() - 1;
    }
    private int getItem(int i) {
        return vFlip.getCurrentItem() + 1;
    }

    public View GetView() {
        return singleStoryDescriptionView;
    }

    public class SingleStorySliderAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            sliderMediaView = layoutInflater.inflate(R.layout.slider_images, container, false);
            VideoView vid = null;
            final String captions[] = CurrentStory.getInstance().getImageCaptions();
            if(captions.length > position)
                sliderDescription.setText(captions[position]);
            else
                sliderDescription.setText(captions[position - 1]);
            String imgs[] = CurrentStory.getInstance().getImages();
            String vids[] = CurrentStory.getInstance().getVideos();
            String img_alts[] = CurrentStory.getInstance().getImageAlts();
            if (position < vidCount) {
                //do videos

                String uriPath = "android.resource://" + conContext.getPackageName() + "/raw/" + vids[position];
                String video = vids[position];
                String thumbs[] = CurrentStory.getInstance().getVideoThumbs();
                int thumbID = conContext.getResources().getIdentifier(thumbs[position], "drawable", conContext.getPackageName());
                //CustomVideoPlayer vidPlayer = new CustomVideoPlayer(conContext, sliderMediaView, uriPath, thumbID);
            } else {
                // custom dialog
                final Dialog diaDialog = new Dialog(conContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                diaDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                //diaDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

                // Include dialog.xml file
                diaDialog.setContentView(R.layout.fullscreen_image);
                int height = (int)(getResources().getDisplayMetrics().heightPixels);
                int width = (int)(getResources().getDisplayMetrics().widthPixels);
                //diaDialog.getWindow().setLayout(width, height);
                //diaDialog.getWindow().getContainer().setLayout(height, width);

                diaDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        diaDialog.hide();
                    }
                });
                //diaDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                //get dialog title textView

                //vP = (EasyVideoPlayer) diaDialog.findViewById(R.id.easyVideo2);
                ImageView diaImg = (ImageView) diaDialog.findViewById(R.id.full_screen_image);

                //player = (EasyVideoPlayer) sliderMediaView.findViewById(R.id.easyVideo);
                diaDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        CurrentStory.getInstance().MainStory.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    }
                });
                //do images
                ImageView img = (ImageView) sliderMediaView.findViewById(R.id.imageView2);
                //TextView text1 = (TextView) v.findViewById(R.id.year);
                //ViewPager vFlip = (ViewPager) v.findViewById(R.id.viewFlipper);

                int img_id = conContext.getResources().getIdentifier(imgs[position-vidCount], "drawable", conContext.getPackageName());
                //ImageView tempImage = new ImageView(conContext);
                //tempImage.setImageResource(img_id);
                //img.setImageResource(img_id);
                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inSampleSize = 4;
//                img.setImageBitmap(BitmapFactory.decodeResource(conContext.getResources(), img_id, opts));

                Glide.with(conContext).load(img_id)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(img);
                img.setVisibility(View.VISIBLE);
                img.setContentDescription(img_alts[position-vidCount]);

                opts.inSampleSize = 4;
                Glide.with(conContext).load(img_id)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(diaImg);
//                diaImg.setImageBitmap(BitmapFactory.decodeResource(conContext.getResources(), img_id, opts));
                diaImg.setContentDescription(img_alts[position-vidCount]);

                diaImg.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        diaDialog.hide();
                    }
                });

                img.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //fullScreen();
                        int uiOptions = activity.getWindow().getDecorView().getSystemUiVisibility();
                        diaDialog.show();

                        int newUiOptions = uiOptions;
                        // END_INCLUDE (get_current_ui_flags)
                        // BEGIN_INCLUDE (toggle_ui_flags)
                        boolean isImmersiveModeEnabled =
                                ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
                        if (isImmersiveModeEnabled) {
                            Log.i("SingleStoryDescription", "Turning immersive mode mode off. ");


                        } else {
                            Log.i("SingleStoryDescription", "Turning immersive mode mode on.");
                        }

                        // Navigation bar hiding:  Backwards compatible to ICS.
                        if (Build.VERSION.SDK_INT >= 14) {
                            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
                        }

                        // Status bar hiding: Backwards compatible to Jellybean
                        if (Build.VERSION.SDK_INT >= 16) {
                            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
                        }

                        // Immersive mode: Backward compatible to KitKat.
                        // Note that this flag doesn't do anything by itself, it only augments the behavior
                        // of HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this sample
                        // all three flags are being toggled together.
                        // Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
                        // Sticky immersive mode differs in that it makes the navigation and status bars
                        // semi-transparent, and the UI flag does not get cleared when the user interacts with
                        // the screen.
                        if (Build.VERSION.SDK_INT >= 18) {
                            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                        }

                        activity.getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
                    }
                });
            }

            container.addView(sliderMediaView);
            return sliderMediaView;

        }

        public void fullScreen() {
            // BEGIN_INCLUDE (get_current_ui_flags)
            // The UI options currently enabled are represented by a bitfield.
            // getSystemUiVisibility() gives us that bitfield.

            int uiOptions = activity.getWindow().getDecorView().getSystemUiVisibility();
            int newUiOptions = uiOptions;
            // END_INCLUDE (get_current_ui_flags)
            // BEGIN_INCLUDE (toggle_ui_flags)
            boolean isImmersiveModeEnabled =
                    ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
            if (isImmersiveModeEnabled) {
                Log.i("SingleStoryDescription", "Turning immersive mode mode off. ");
            } else {
                Log.i("SingleStoryDescription", "Turning immersive mode mode on.");
            }

            // Navigation bar hiding:  Backwards compatible to ICS.
            if (Build.VERSION.SDK_INT >= 14) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            }

            // Status bar hiding: Backwards compatible to Jellybean
            if (Build.VERSION.SDK_INT >= 16) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
            }

            // Immersive mode: Backward compatible to KitKat.
            // Note that this flag doesn't do anything by itself, it only augments the behavior
            // of HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this sample
            // all three flags are being toggled together.
            // Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
            // Sticky immersive mode differs in that it makes the navigation and status bars
            // semi-transparent, and the UI flag does not get cleared when the user interacts with
            // the screen.
            if (Build.VERSION.SDK_INT >= 18) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            }

            activity.getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
            //END_INCLUDE (set_ui_flags)

        }

        @Override
        public int getCount() {
            return mediaCount;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

            // remove previous slide
            View v = (View) object;

            //Todo: Image Slider: Improve memory (skip frames error when swiping really fast)
            v.setBackgroundResource(0);

            container.removeView(v);
        }

    }
}
