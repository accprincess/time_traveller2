package ca.accurate.timetraveller;


import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.data.Point;
import com.google.maps.android.ui.IconGenerator;

/**
 * Created by P on 2017-01-12.
 */

public class MapFragmentView extends FrameLayout implements OnMapReadyCallback, com.google.android.gms.location.LocationListener, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ClusterManager.OnClusterItemClickListener<MapFragmentView.MyItem> {
    private static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap gMap;
    private Marker[] marMarkers;
    private Bitmap[] bitBitmapDefaults, bitBitmapHighlights;
    private LocationRequest locLocationRequest;
    private View v;
    private MapViewPager mapViewPager;
    private FragmentManager fragmentManager;
    private MapView gMapView;
    private Context conContext;
    private Button mapReadStory;
    private LayoutInflater layoutInflater;
    private TimelineMainViewPager timelineMainViewPager;
    private LinearLayout showPin, showTitle;
    private TextView mapTitle;
    private int i;
    private int currentMarkerClicked;
    // Declare a variable for the cluster manager.
    private ClusterManager<MyItem> mClusterManager;
    private ClusterRenderer mClusterRenderer;
    private Marker prevMarker, currMarker;

    public MapFragmentView(Context context, LayoutInflater inflater, ViewGroup container, FragmentManager fManager, MapViewPager mVPager, TimelineMainViewPager tmMainVP) {
        super(context);
        layoutInflater = inflater;
        timelineMainViewPager = tmMainVP;
        conContext = context;

        v = layoutInflater.inflate(R.layout.fragment_map_view, container, false);
        //InitMap();
        mapViewPager =  mVPager;
        fragmentManager = fManager;

        gMapView = (MapView) v.findViewById(R.id.map_fragment);
        gMapView.getMapAsync(this);
//        SupportMapFragment mapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map_fragment);
//        mapFragment.getMapAsync(this);

        gMapView.onCreate(null);
        gMapView.onResume(); // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(conContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapReadStory = (Button) v.findViewById(R.id.map_read_story);
        mapTitle = (TextView) v.findViewById(R.id.map_title);

        this.addView(v);
    }


    @Override
    public void onLocationChanged(Location location) {
        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }



    @Override
    public boolean onMarkerClick(final Marker marker) {
//        int index = -1;
//        for (i = 0; i < marMarkers.length; i++)
//        {
//            //find the marker that was clicked
//            if (marMarkers[i].getTitle().equals(marker.getTitle()) ) {
//                index = i;
//
//                String year = CurrentStory.getInstance().getSpecificYear(i);
//                String title = CurrentStory.getInstance().getSpecificTitle(i);
//
//                // put year and title with different colors
//                SpannableStringBuilder builder = new SpannableStringBuilder();
//
//                // style the year
//                SpannableString whiteYear= new SpannableString(year);
//                whiteYear.setSpan(new ForegroundColorSpan(Color.WHITE), 0, year.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//                builder.append(whiteYear);
//
//                // style the title
//                SpannableString blackTitle = new SpannableString(title);
//                blackTitle.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//                builder.append(" " + blackTitle);
//
//                Typeface tfAbril = Typeface.createFromAsset(getContext().getAssets(), "fonts/AbrilFatface-Regular.ttf");
//
//                // show title with year in white
//                mapTitle.setText(builder);
//                mapTitle.setTypeface(tfAbril);
//
//            }
//
//            //reset all markers to default icon
//            marMarkers[i].setIcon(BitmapDescriptorFactory.fromBitmap(bitBitmapDefaults[i]));
//        }
//
//        //set icon of clicked marker to highlight
//        marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitBitmapHighlights[index]));
//
//        mapReadStory.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int index = -1;
//                for (i = 0; i < marMarkers.length; i++) {
//                    if (marMarkers[i].getTitle().equals(marker.getTitle()) ) {
//                        index = i;
//                        String title = CurrentStory.getInstance().getSpecificTitle(i);
//                        int index2 = CurrentStory.getInstance().FindStoryByTitle(title);
//                        CurrentStory.getInstance().setCurrentStory(Integer.toString(index2 + 1), conContext);
//                        mapViewPager.setCurrentItem(CurrentStory.STORY_VIEW);
//                    }
//
//                }
//            }
//        });
//
//        // Hide and Show Read Story at the bottom
//        showPin = (LinearLayout) v.findViewById(R.id.showPin);
//        showTitle = (LinearLayout) v.findViewById(R.id.linear_map_title);
//        showPin.setVisibility(View.VISIBLE);
//        showTitle.setVisibility(View.VISIBLE);
//
//        //Hide title snippet when click
//        marker.hideInfoWindow();
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        gMap = googleMap;
        setUpClusterer();


        //try styling the gmaps (as there could be issues on some version of Android
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

//        //get the number of stories
//        int iCount = CurrentStory.getInstance().getStoryCount();
//
//        //setup, keep all markers and bitmaps in array so we don't need to recreate any new data
//        marMarkers = new Marker[iCount];
//        bitBitmapDefaults = new Bitmap[iCount];
//        bitBitmapHighlights = new Bitmap[iCount];
//
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                gMap.setMyLocationEnabled(true);

            }
        }
        else {
            buildGoogleApiClient();
            gMap.setMyLocationEnabled(true);

        }
//
//        //loop through each story and put its location pin on the map
//        for(int i = 0; i < iCount; i++) {
//            //get lat/long
//            String loc[] = CurrentStory.getInstance().getSpecificLocation(i);
//            if (loc != null) {
//                //convert lat/long string to float
//                LatLng llStory = new LatLng(Float.parseFloat(loc[0]), Float.parseFloat(loc[1]));
//
//                //create default and highlight bitmaps for icons
//                Bitmap bMapIconDefault = createMapIcon(CurrentStory.getInstance().getSpecificYear(i), Color.rgb(188, 155, 81), true);
//                Bitmap bMapIconHighlight = createMapIcon(CurrentStory.getInstance().getSpecificYear(i), Color.rgb(255, 255, 255), false);
//
//                //create/add marker to gmap
               //Marker mk = gMap.addMarker(new MarkerOptions().position(llStory).title(CurrentStory.getInstance().getSpecificTitle(i)).snippet(CurrentStory.getInstance().getSpecificYear(i)).icon(BitmapDescriptorFactory.fromBitmap(bMapIconDefault)) );
//
//                //save marker and bitmaps into their arrays
//                //marMarkers[i] = mk;
//                bitBitmapDefaults[i] = bMapIconDefault;
//                bitBitmapHighlights[i] = bMapIconHighlight;
//
//                //setUpClusterer();
//

//
//
//            }
//        }
//        //initialize camera position to first marker that was added
////        gMap.moveCamera(CameraUpdateFactory.newLatLng(marMarkers[i].getPosition()));
//        gMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(45.3917193,
//                -75.6798936)));
//        //gMap.moveCamera(CameraUpdateFactory.zoomTo(13));
//        gMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
//
//        //add click listeners
//        gMap.setOnMarkerClickListener(this);
//        gMap.setOnMapClickListener(this);
//        //gMap.setOnMyLocationButtonClickListener(this);
    }

    //I put this in a function since it's used to create both highlight and default icons. could be moved or
    //repurposed into a new class or function if needed. Perhaps create a singleton class to hold all the bitmaps?
    //we'll see how it affects performance
    private Bitmap createMapIcon(String year, int color, Boolean isDefault) {
        Bitmap icon;

        //Get screen size
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);

        Display display = wm.getDefaultDisplay();
        String displayName = display.getName();
        // pixels, dpi
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        int widthPixels = metrics.widthPixels;
        //Log.i(TAG, "widthPixels  = " + widthPixels);

        int bitmapWidth = 160;
        int fontSize = 0;
        if(widthPixels == 1440){
            bitmapWidth = 800;
        }
        else if(widthPixels == 1080){
            bitmapWidth = 600;
        }
        else if(widthPixels == 768) {
            bitmapWidth = 300;
        }
        else if (widthPixels == 480){
            bitmapWidth = 160;
        }
        else{
            bitmapWidth = 160;
        }

        //get the png graphic based on if we want the default or highlighted icon states
        if (isDefault)
            icon = BitmapFactory.decodeResource(getResources(), R.drawable.map_icon_default);
        else
            icon = BitmapFactory.decodeResource(getResources(), R.drawable.map_icon_white);

        //create bitmap from the png
        //Bitmap bMapScaled = Bitmap.createScaledBitmap(icon, pxToDp(166), pxToDp(265), true);
        Bitmap bMapScaled = Bitmap.createScaledBitmap(icon, pxToDp(bitmapWidth), pxToDp(bitmapWidth+130), true);
        //create canvas so we can add text
        Bitmap workingBitmap = Bitmap.createBitmap(bMapScaled);
        bMapScaled = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(bMapScaled);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //color from user
        paint.setColor(color);
        paint.setShadowLayer(5.0f, 5.0f, 5.0f, Color.BLACK);

        // text size in pixels
        double scale = Resources.getSystem().getDisplayMetrics().density / 1.5;
        float size = 26 * (float) scale;
        paint.setTextSize( size );


        // draw text to the top of the Canvas
        Rect bounds = new Rect();
        paint.getTextBounds(year, 0, year.length(), bounds);
        //set font
        //paint.setTypeface(CustomFont.getInstance().AbrilFont(getBaseContext(), "abril" ));
        paint.setTypeface(CustomFont.getInstance().AbrilFont(getContext(), "abril" ));
        //set position (center top)
        int x = (bMapScaled.getWidth() - bounds.width())/2;
        int y = bounds.height();
        //draw text on icon
        canvas.drawText(year, x, y, paint);

        icon = null;
        workingBitmap = null;
        canvas = null;
        bounds = null;
        //return finished bitmap icon with png and year text
        return bMapScaled;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.setAlwaysShow(true);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locLocationRequest = new LocationRequest();
        locLocationRequest.setInterval(1000);
        locLocationRequest.setFastestInterval(1000);
        locLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locLocationRequest, this);
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getContext(), "Connection suspended, please unsuspend.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getContext(), "GPS not enabled, please enable.", Toast.LENGTH_LONG).show();
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
    public View GetView() {
        return v;
    }



    @Override
    public boolean onClusterItemClick(final MyItem item) {
        //MyItem newItem = item;
        currMarker = mClusterRenderer.getMarker(item);
        currMarker.setIcon(BitmapDescriptorFactory.fromBitmap(createMapIcon(CurrentStory.getInstance().getSpecificYear(item.getIndex()), Color.rgb(255, 255, 255), false)));
        if (prevMarker != null)
            prevMarker.setIcon(BitmapDescriptorFactory.fromBitmap(createMapIcon(CurrentStory.getInstance().getSpecificYear(item.getIndex()), Color.rgb(188, 155, 81), true)));

        String year = CurrentStory.getInstance().getSpecificYear(item.mIndex);
        String title = CurrentStory.getInstance().getSpecificTitle(item.mIndex);

        // put year and title with different colors
        SpannableStringBuilder builder = new SpannableStringBuilder();

        // style the year
        SpannableString whiteYear = new SpannableString(year);
        whiteYear.setSpan(new ForegroundColorSpan(Color.WHITE), 0, year.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(whiteYear);

        // style the title
        SpannableString blackTitle = new SpannableString(title);
        blackTitle.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(" " + blackTitle);

        Typeface tfAbril = Typeface.createFromAsset(getContext().getAssets(), "fonts/AbrilFatface-Regular.ttf");

        // show title with year in white
        mapTitle.setText(builder);
        mapTitle.setTypeface(tfAbril);
        year = null;
        title = null;
        builder = null;
        whiteYear = null;
        blackTitle = null;
        tfAbril = null;
//
            mapReadStory.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

             //String title = CurrentStory.getInstance().getSpecificTitle(item.mIndex);
                int index2 = CurrentStory.getInstance().FindStoryByTitle(item.getTitle());
                CurrentStory.getInstance().setCurrentStory(Integer.toString(index2 + 1), conContext);
                mapViewPager.setCurrentItem(CurrentStory.STORY_VIEW);
            }
        });


        // Hide and Show Read Story at the bottom
        showPin = (LinearLayout) v.findViewById(R.id.showPin);
        showTitle = (LinearLayout) v.findViewById(R.id.linear_map_title);
        showPin.setVisibility(View.VISIBLE);
        showTitle.setVisibility(View.VISIBLE);

        // Does nothing, but you could go into the user's profile page, for example.
        prevMarker = currMarker;
        currMarker.hideInfoWindow();
        return false;
    }


    private class ClusterRenderer extends DefaultClusterRenderer<MyItem> {
        //public MarkerCache<T> mMarkerCache = new MarkerCache<T>();
        private final IconGenerator mClusterIconGenerator = new IconGenerator(conContext);

        public ClusterRenderer() {
            super(conContext, gMap, mClusterManager);

        }

//        public Marker getMarker(MyItem clusterItem) {
//            return getMarker(clusterItem);
//        }

        @Override
        protected void onBeforeClusterItemRendered(MyItem person, MarkerOptions markerOptions) {
            for(int i = 0; i < person.mIndex + 1; i++) {
                Bitmap bMapIconDefault;
                if (person.getIndex() != currentMarkerClicked) {
                    bMapIconDefault = createMapIcon(CurrentStory.getInstance().getSpecificYear(i), Color.rgb(188, 155, 81), true);
                } else {
                    bMapIconDefault = createMapIcon(CurrentStory.getInstance().getSpecificYear(i), Color.rgb(255, 255, 255), false);
                }
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bMapIconDefault));
                //markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_icon_default));

                bMapIconDefault = null;
            }
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<MyItem> cluster, MarkerOptions markerOptions) {
            final Drawable clusterIcon = getResources().getDrawable(R.drawable.circle);
            clusterIcon.setColorFilter(Color.parseColor("#D6B45B"), PorterDuff.Mode.SRC_ATOP);

            mClusterIconGenerator.setBackground(clusterIcon);
            //mClusterIconGenerator.setColor(Color.parseColor("#D6B45B"));
            mClusterIconGenerator.setTextAppearance(R.style.iconGenText);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));

            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            return cluster.getSize() > 1;
        }
    }


    public class MyItem implements ClusterItem {
        private final LatLng mPosition;
        private  String mTitle;
        private  String mSnippet;
        private Integer mIndex;
        private String mYear;

        public MyItem(double lat, double lng) {
            mPosition = new LatLng(lat, lng);
        }

        public MyItem(double lat, double lng, String title, String snippet, Integer index) {
            mPosition = new LatLng(lat, lng);
            mTitle = title;
            mSnippet = snippet;
            mIndex = index;
        }

        public void SetYear(String year) { mYear = year; }
        public String GetYear() { return mYear; }

        @Override
        public LatLng getPosition() {
            return mPosition;
        }

        @Override
        public String getTitle() {
            return mTitle;
        }

        @Override
        public String getSnippet() {
            return mSnippet;
        }

        //@Override
        public Integer getIndex() {
            return mIndex;
        }

    }


    private void setUpClusterer() {
        // Position the map.
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(45.3917193, -75.6798936), 10));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)
        mClusterManager = new ClusterManager<MyItem>(conContext,gMap);
        mClusterRenderer = new ClusterRenderer();
        mClusterManager.setRenderer(mClusterRenderer);
        mClusterManager.setOnClusterItemClickListener(this);

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        gMap.setOnCameraIdleListener(mClusterManager);
        gMap.setOnMarkerClickListener(mClusterManager);

        // Add cluster items (markers) to the cluster manager.
        addItems();
    }

    private void addItems() {

        //get the number of stories
        int iCount = CurrentStory.getInstance().getStoryCount();
        for(int i = 0; i < iCount; i++) {
            //get lat/long
            String loc[] = CurrentStory.getInstance().getSpecificLocation(i);
            //Log.d("MapFragmentView:", "i = " + Integer.toString(i));
            if (loc.length>0) {
                //create default and highlight bitmaps for icons
                //Bitmap bMapIconDefault = createMapIcon(CurrentStory.getInstance().getSpecificYear(i), Color.rgb(188, 155, 81), true);

                MyItem offsetItem = new MyItem(Double.parseDouble(loc[0]), Double.parseDouble(loc[1]), CurrentStory.getInstance().getSpecificTitle(i),CurrentStory.getInstance().getSpecificYear(i), i);
                offsetItem.SetYear(CurrentStory.getInstance().getSpecificYear(i));
                mClusterManager.addItem(offsetItem);
                //bMapIconDefault = null;

            }
        }
    }



}

