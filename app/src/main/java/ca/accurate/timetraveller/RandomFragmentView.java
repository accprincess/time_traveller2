package ca.accurate.timetraveller;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.VideoView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;

/**
 * Created by P on 2017-02-09.
 */

public class RandomFragmentView extends FrameLayout {


        private Context conContext;
        private LayoutInflater layoutInflater;
        private View fragmentRandomView;
        private ViewGroup viewGroup;
        private VideoView mVideoView;
        private AHBottomNavigation bottomNavigation;

        public RandomFragmentView(Context context, LayoutInflater inflater, ViewGroup container, FragmentManager fManager) {
            super(context);
            conContext = context;
            //layoutInflater = inflater;
            layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            viewGroup = container;
            fragmentRandomView = layoutInflater.inflate(R.layout.fragment_random, container, false);
            //mVideoView.start();
            this.addView(fragmentRandomView);
        }

    public void SetBottomNav(AHBottomNavigation nav) {
        bottomNavigation = nav;
    }

        public void refresh() {
            mVideoView = (VideoView) fragmentRandomView.findViewById(R.id.videoView);
            mVideoView.setVideoPath("android.resource://"+MainAppActivity.PACKAGE_NAME+"/raw/main_1000x1000px");

            //mVideoView.setMediaController(new MediaController(this));
            mVideoView.seekTo(1);
            mVideoView.setVisibility(View.VISIBLE);
            mVideoView.start();
            //refreshBottomNav();
        }
        public void refreshBottomNav() {
            new Handler().postDelayed(Variables.getInstance().randomDelay = new Runnable(){
                @Override
                public void run() {
                    /* Create an Intent that will start the Menu-Activity. */
                    //refreshBottomNav();
                    bottomNavigation.setCurrentItem(1);
                    bottomNavigation.refresh();
                    MainAppActivity.GoToTimelineView();

                }
            }, 2000);
        }


}
