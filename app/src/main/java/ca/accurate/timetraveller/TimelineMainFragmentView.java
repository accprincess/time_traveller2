package ca.accurate.timetraveller;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by P on 2017-01-24.
 */

public class TimelineMainFragmentView extends FrameLayout {

    private Context conContext;
    private LayoutInflater layoutInflater;
    private ViewGroup viewGroup;
    private FragmentManager fragmentManager;
    private TimelineMainViewPager timelineMainViewPager;
    private View fragmentTimelineView;
    private TimelineMainAdapter tmAdapter;
    private MapViewPager mapViewPager;

    public TimelineMainFragmentView(Context context, LayoutInflater inflater, ViewGroup container, FragmentManager fManager) {
        super(context);
        conContext = context;
        //layoutInflater = inflater;
        layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewGroup = container;
        fragmentManager = fManager;

        fragmentTimelineView = layoutInflater.inflate(R.layout.fragment_timeline_view, container, false);

        timelineMainViewPager = (TimelineMainViewPager) fragmentTimelineView.findViewById(R.id.timeline_main_viewpager);
        tmAdapter = new TimelineMainAdapter(conContext, layoutInflater, fragmentManager, timelineMainViewPager, mapViewPager);
        timelineMainViewPager.setAdapter(tmAdapter);
        //timelineMainViewPager.setOffscreenPageLimit(4);

        this.addView(fragmentTimelineView);
    }

    public void ResetData() {
        tmAdapter.ResetData();
    }

    public void GoToTimelineView() {
        tmAdapter.GoToTimelineView();
    }

    public void GoToStoryView() {
        tmAdapter.GoToStoryView();
    }

    public int getPosition(){ return tmAdapter.getPosition();}

    public TimelineMainViewPager timelineMainViewPager(){
        return timelineMainViewPager;
    }
}
