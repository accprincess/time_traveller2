package ca.accurate.timetraveller;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by Spencers on 31/01/2017.
 */

public class MapMainFragmentView extends FrameLayout {

    private Context conContext;
    private LayoutInflater layoutInflater;
    private ViewGroup viewGroup;
    private FragmentManager fragmentManager;
    private MapViewPager mapMainViewPager;
    private View mapMainView;
    private MapMainAdapter mapAdapter;
    private TimelineMainViewPager timelineMainViewPager;

    public MapMainFragmentView(Context context, LayoutInflater inflater, ViewGroup container, FragmentManager fManager) {
        super(context);
        conContext = context;
        //layoutInflater = inflater;
        layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewGroup = container;
        fragmentManager = fManager;

        mapMainView = layoutInflater.inflate(R.layout.fragment_map_main_view, container, false);

        mapMainViewPager = (MapViewPager) mapMainView.findViewById(R.id.map_main_viewpager);
        mapAdapter = new MapMainAdapter(conContext, layoutInflater, fragmentManager, mapMainViewPager, timelineMainViewPager);
        mapMainViewPager.setAdapter(mapAdapter);


        //timelineMainViewPager.setOffscreenPageLimit(4);

        this.addView(mapMainView);
    }

    public void ResetData() {
        mapAdapter.ResetData();
    }

    public void GoToTimelineView() {
        mapAdapter.GoToTimelineView();
    }

    public void GoToStoryView() {
        mapAdapter.GoToStoryView();
    }

    public int getPosition(){ return mapAdapter.getPosition(); }

    public MapViewPager getMapMainViewPager(){
        return mapMainViewPager;
    }
}
