package ca.accurate.timetraveller;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Princess on 2017-02-20.
 */

public class NotificationMainAdapter extends PagerAdapter {

    private Context conContext;
    private LayoutInflater layoutInflater;
    private FragmentManager fragmentManager;
    private NotificationViewPager notificationViewPager;
    private SingleStorySourceView sourceView;
    private SingleStoryMapView mapView;
    private SingleStory story;
    private SingleStoryDescriptionView descView;
    private TimelineMainViewPager tmMainSlider;
    private MapViewPager mapViewPager;
    private int pos;

    public NotificationMainAdapter(Context con, LayoutInflater inflater, FragmentManager fManager, NotificationViewPager nViewPager) {
        conContext = con;
        layoutInflater = inflater;
        fragmentManager = fManager;
        notificationViewPager = nViewPager;

        notificationViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch(position) {
                    case 0:
                        pos = 0;
                        break;
                    case 1:
                        pos = 1;
                        break;
                    case 2:
                        pos = 2;
                        break;
                    case 3:
                        pos = 3;
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = null;
        switch (position) {
            case 0:
                story = new SingleStory(conContext, container, tmMainSlider, mapViewPager, notificationViewPager);
                story.ResetData();
                v = story.GetView();
                break;
            case 1:
                descView = new SingleStoryDescriptionView(conContext, container, layoutInflater, fragmentManager, tmMainSlider, mapViewPager, notificationViewPager);
                descView.ResetData();
                v = descView.GetView();
                break;
            case 2:
                sourceView = new SingleStorySourceView(conContext, container, layoutInflater, fragmentManager, tmMainSlider, mapViewPager, notificationViewPager);
                sourceView.ResetData();
                v = sourceView.GetView();
                break;
            case 3:
                //v = InitSingleStoryMap(container);
                mapView = new SingleStoryMapView(conContext, container, layoutInflater, fragmentManager, tmMainSlider, mapViewPager, notificationViewPager);
                mapView.ResetData();
                v = mapView.GetView();
                break;
//            default:
//                //v = InitSingleStoryMap(container);
//                break;
        }

        if( v.getParent()!=null)
            ((ViewGroup)v.getParent()).removeView(v); // <- fix
        container.addView(v);

        return v;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        // remove previous slide
        View v = (View) object;

        //Todo: Image Slider: Improve memory (skip frames error when swiping really fast)
        v.setBackgroundResource(0);

        container.removeView(v);
    }

    public void GoToTimelineView() {
        //settingsSlider.GoToTimelineView();
    }

    public void ResetData() {
        //settingsSlider.ResetData();
    }

    public void GoToStoryView() {
        //settingsSlider.GoToStoryView();
    }

    public int getPosition(){
        return pos;
    }

}
