package ca.accurate.timetraveller;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by P on 2017-01-12.
 */

public class MainSettingsActivity extends FrameLayout{

    private ViewGroup viewGroup;
    private FragmentManager fragmentManager;
    private Context conContext;
    private View mainSettingsView;
    private SettingsViewPager settingsViewPager;



    public MainSettingsActivity(final Context context, LayoutInflater inflater, ViewGroup container, FragmentManager fManager, SettingsViewPager setViewPager) {
        super(context);
        settingsViewPager = setViewPager;
        mainSettingsView = inflater.inflate(R.layout.fragment_demo_settings, container, false);
        final MainAppActivity demoActivity = (MainAppActivity) context;
        //final SwitchCompat switchLanguage = (SwitchCompat) mainSettingsView.findViewById(R.id.fragment_demo_switch_language);
        final SwitchCompat notification_on = (SwitchCompat) mainSettingsView.findViewById(R.id.notification_on);
        final SwitchCompat language_on = (SwitchCompat) mainSettingsView.findViewById(R.id.language_on);
        //final SwitchCompat switchNotifications = (SwitchCompat) mainSettingsView.findViewById(R.id.switch_notifications);
        TextView textLanguage = (TextView) mainSettingsView.findViewById(R.id.textLanguage);
        TextView textNotification = (TextView) mainSettingsView.findViewById(R.id.textNotification);
        TextView textSettings = (TextView) mainSettingsView.findViewById(R.id.textSettings);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Spinnaker-Regular.ttf");
        Typeface tfAbril = Typeface.createFromAsset(getContext().getAssets(), "fonts/AbrilFatface-Regular.ttf");
        Button helpBtn = (Button) mainSettingsView.findViewById(R.id.help_button);
        Button aboutBtn = (Button) mainSettingsView.findViewById(R.id.about_button);


        textLanguage.setTypeface(tf);
        notification_on.setTypeface(tf);
        language_on.setTypeface(tf);
        textNotification.setTypeface(tf);
       // switchLanguage.setTypeface(tf);
        //switchNotifications.setTypeface(tf);
        textSettings.setTypeface(tfAbril);
        helpBtn.setTypeface(tf);
        aboutBtn.setTypeface(tf);



        language_on.setChecked(context
                .getSharedPreferences("shared", Context.MODE_PRIVATE)
                .getBoolean("language", true));

//        notification_on.setVisibility(
//                Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? View.VISIBLE : View.GONE);

        context
                .getSharedPreferences("isSwitch", Context.MODE_PRIVATE)
                .edit()
                .putBoolean("isSwitch", false)
                .apply();

        context
                .getSharedPreferences("tutorialSwitch", Context.MODE_PRIVATE)
                .edit()
                .putBoolean("tutorialSwitch", false)
                .apply();

        language_on.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                context
                        .getSharedPreferences("shared", Context.MODE_PRIVATE)
                        .edit()
                        .putBoolean("language", isChecked)
                        .apply();

                context
                        .getSharedPreferences("isSwitch", Context.MODE_PRIVATE)
                        .edit()
                        .putBoolean("isSwitch", true)
                        .apply();
                if(!isChecked){
                    demoActivity.setLocale("fr");
                    demoActivity.reload();
                }else{
                    demoActivity.setLocale("en");
                    demoActivity.reload();
                }

            }

        });

        //Notifications

        notification_on.setChecked(context
                .getSharedPreferences("switchNotifications", Context.MODE_PRIVATE)
                .getBoolean("switchNotifications", false));
        context
                .getSharedPreferences("isSwitchN", Context.MODE_PRIVATE)
                .edit()
                .putBoolean("isSwitchN", false)
                .apply();

        notification_on.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                boolean isSwitchN = true;

                context
                        .getSharedPreferences("switchNotifications", Context.MODE_PRIVATE)
                        .edit()
                        .putBoolean("switchNotifications", isChecked)
                        .apply();
                context
                        .getSharedPreferences("isSwitchN", Context.MODE_PRIVATE)
                        .edit()
                        .putBoolean("isSwitchN", isSwitchN)
                        .apply();
                demoActivity.reload();
            }

        });

        helpBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                settingsViewPager.setCurrentItem(1);
            }
        });

        aboutBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsViewPager.setCurrentItem(2);
            }
        });


        this.addView(mainSettingsView);
    }

    public View GetView() {
        return mainSettingsView;
    }



}
