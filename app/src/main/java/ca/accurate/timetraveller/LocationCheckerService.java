package ca.accurate.timetraveller;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.Arrays;
import java.util.List;

public class LocationCheckerService extends Service
{
    private static final String TAG = "LocationCheckerService";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final int LOCATION_CHECKER_INTERVAL = 10000;
    private static final int LOCATION_CHECKER_DURATION = 60000; // 1 min total countdown time
    private static final float LOCATION_DISTANCE = 10f;
    private List<String> mMatchingProviders;
    private Location mLocation;
    private String[] mPins;
    private Boolean[] mRead;
    private static String GROUP_EVENTS = "events";

    private CountDownTimer mCountDownTimer;

    private class LocationListener implements android.location.LocationListener
    {
        private Location mLastLocation;

        private LocationListener(String provider)
        {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location)
        {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
        }

        @Override
        public void onProviderDisabled(String provider)
        {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider)
        {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    private LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0)
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        mCountDownTimer = new CountDownTimer(LOCATION_CHECKER_DURATION, LOCATION_CHECKER_INTERVAL) {
            @Override
            public void onTick(long l) {
                Log.i(TAG, "countdown ticking");
            }

            @Override
            public void onFinish() {
                // Start again
                newObtainLocation();
                mCountDownTimer.start();
            }
        }.start();

        return START_STICKY;
    }

    @Override
    public void onCreate()
    {
        Log.e(TAG, "onCreate");

        mPins = getApplicationContext().getResources().getStringArray(R.array.locations);
        mRead = new Boolean[mPins.length];
        Arrays.fill(mRead, Boolean.FALSE);

        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy()
    {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        if (mLocationManager != null) {
            for (LocationListener locationListener : mLocationListeners) {
                try {
                    mLocationManager.removeUpdates(locationListener);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private void newObtainLocation() {
        Log.i(TAG, "Getting new location");
        mMatchingProviders = mLocationManager.getAllProviders();

        for (String provider : mMatchingProviders) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mLocation = mLocationManager.getLastKnownLocation(provider);
                if (mLocation != null) {

                    CurrentStory currentStory = CurrentStory.getInstance();
                    for (int i = 0; i < mPins.length; i++) {

                        String title = currentStory.getSpecificTitle(i);
                        String year = currentStory.getSpecificYear(i);

                        //pin location
                        String[] loc = currentStory.getSpecificLocation(i);
                        float lat = Float.parseFloat(loc[0]);
                        float lng = Float.parseFloat(loc[1]);
                        Location l = new Location("pin location");
                        l.setLatitude(lat);
                        l.setLongitude(lng);

                        // calculate distance between user and pins
                        float distance = mLocation.distanceTo(l);
                        int d = Math.round(distance);
                        // System.out.println(d);
                        // send notification of the closest pins only
                        if (d < 300) { // distance is in Meters

                            if (mRead[i].equals(false)) {
                                sendNotification(title, year, mRead[i], i, d, getApplicationContext());
                                mRead[i] = true;
                            }
                        }
                    }
                }
            }
        }
    }

    private void sendNotification(String title, String year, boolean read, int id, int distance, Context context) {
        //cContext = c;
        int color = ContextCompat.getColor(getBaseContext(), R.color.notificationAccent);

        NotificationManager manager = (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

        //Intent notificationIntent = new Intent(getBaseContext(), MainAppActivity.class);
        Intent notificationIntent = new Intent(getBaseContext(), MainAppActivity.class);
        notificationIntent.putExtra("storyId", id);
        notificationIntent.putExtra("fromNotification", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Notification builder = new NotificationCompat.Builder(getBaseContext())
                .setLargeIcon(BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.notification_icon))
                .setSmallIcon(R.drawable.nav_timeline_icon_highlighted) // needs the small icon to work properly & needs to work as white
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.DEFAULT_ALL)
                .setContentTitle("Event is Nearby! ")
                .setContentIntent(pendingIntent)
                .setColor(color)
                .setOngoing(false)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setGroup(GROUP_EVENTS)
                .setContentText("It's " + distance + "m close to you")
                .setGroupSummary(true)
                .addAction(R.drawable.nav_timeline_icon_default, title, pendingIntent)
                .build();

        boolean enabledNotifications = getSharedPreferences("switchNotifications", Context.MODE_PRIVATE)
                .getBoolean("switchNotifications", false);

        if(enabledNotifications){
            manager.notify(id, builder);
        }

    }
}
