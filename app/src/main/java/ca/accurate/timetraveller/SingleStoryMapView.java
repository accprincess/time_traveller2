package ca.accurate.timetraveller;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by P on 2017-01-24.
 */

public class SingleStoryMapView extends FrameLayout implements OnMapReadyCallback, com.google.android.gms.location.LocationListener, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    //private LayoutInflater layoutInflater;
    private ViewGroup viewGroup;
    private FragmentManager fragmentManager;
    private Context conContext;
    private View browseTimelineView;
    private TimelineMainViewPager timelineMainViewPager;
    private MapViewPager mapViewPager;
    private GoogleApiClient mGoogleApiClient;
    private MapView gMapView;
    private GoogleMap gMap;
    private Marker[] marMarkers;
    private Bitmap[] bitBitmapDefaults, bitBitmapHighlights;
    private LocationRequest locLocationRequest;
    private View gMapSingleStoryView;
    private ImageButton btnMyLocation;
    private NotificationViewPager notificationViewPager;
    Location mLastLocation;
    double lat =0, lng=0;
    private BitmapDescriptor markerDescriptor;
    private boolean drawAccuracy = true;
        private Marker positionMarker;
    private Circle accuracyCircle;
    private Bitmap mDotMarkerBitmap;

    public SingleStoryMapView(Context context, ViewGroup container, LayoutInflater inflater, FragmentManager fManager, TimelineMainViewPager tmMainVP, MapViewPager mVPager, NotificationViewPager NViewPager) {
        super(context);

        timelineMainViewPager = tmMainVP;
        mapViewPager = mVPager;
        notificationViewPager = NViewPager;
        conContext = context;
        viewGroup = container;
        //layoutInflater = inflater;
        fragmentManager = fManager;
        LayoutInflater layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        gMapSingleStoryView = layoutInflater.inflate(R.layout.map_only, container, false);
        //SupportMapFragment mapFragment = (SupportMapFragment) v.findViewById(R.id.);
        //mapFragment.getMapAsync(this);
        gMapView = (MapView) gMapSingleStoryView.findViewById(R.id.soleViewMap);
        gMapView.getMapAsync(this);

//        Button backBtn = (Button) gMapSingleStoryView.findViewById(R.id.back_button);
//        backBtn.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                timelineMainViewPager.setCurrentItem(CurrentStory.getInstance().STORY_VIEW);
//            }
//        });

        gMapView.onCreate(null);
        gMapView.onResume(); // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(conContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //if(gMapSingleStoryView.getParent()!=null)
        //    ((ViewGroup)gMapSingleStoryView.getParent()).removeView(gMapSingleStoryView); // <- fix
        this.addView(gMapSingleStoryView);

//        if(gMapView.getParent()!=null)
//            ((ViewGroup)gMapView.getParent()).removeView(gMapView); // <- fix
//        viewGroup.addView(gMapView);

        //container.addView(gMapSingleStoryView);
        //this.addView(gMapSingleStoryView);
    }

    public void ResetData() {

    }

    public View GetView() {
        return gMapSingleStoryView;
    }


    @Override
    public void onLocationChanged(Location location) {


        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        float accuracy = location.getAccuracy();

        if (positionMarker != null) {
            positionMarker.remove();
        }

        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.gold_dot);

        int px = getResources().getDimensionPixelSize(R.dimen.map_location_marker_size);
        mDotMarkerBitmap = Bitmap.createBitmap(px, px, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mDotMarkerBitmap);
        Drawable shape = getResources().getDrawable(R.drawable.gold_dot);
        shape.setBounds(0, 0, mDotMarkerBitmap.getWidth(), mDotMarkerBitmap.getHeight());
        shape.draw(canvas);

        final MarkerOptions positionMarkerOptions = new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .icon(BitmapDescriptorFactory.fromBitmap(mDotMarkerBitmap))
                .anchor(0f, 0f);
        positionMarker = gMap.addMarker(positionMarkerOptions);




            }

@Override
public void onMapClick(LatLng latLng) {

        }

@Override
public boolean onMarkerClick(Marker marker) {
        int index = -1;

        marker.hideInfoWindow();
        return true;
        }

@Override
public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;

        //get the number of stories
        int iCount = CurrentStory.getInstance().getStoryCount();

        //setup, keep all markers and bitmaps in array so we don't need to recreate any new data
        marMarkers = new Marker[iCount];
        bitBitmapDefaults = new Bitmap[iCount];
        bitBitmapHighlights = new Bitmap[iCount];

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (ContextCompat.checkSelfPermission(getContext(),
        android.Manifest.permission.ACCESS_FINE_LOCATION)
        == PackageManager.PERMISSION_GRANTED) {
        buildGoogleApiClient();
        gMap.setMyLocationEnabled(false);


        }
        }
        else {
        buildGoogleApiClient();
        gMap.setMyLocationEnabled(false);
        }

        // hide the my Location button, added our own in XML
        gMap.getUiSettings().setMyLocationButtonEnabled(false);

        //loop through each story and put its location pin on the map
        //for(int i = 0; i < iCount; i++) {
        //get lat/long
        //String loc[] = CurrentStory.getInstance().getSpecificLocation(i);
        String loc[] = CurrentStory.getInstance().getLocation();
        Marker mk;
        if (loc != null) {
        //convert lat/long string to float
        LatLng llStory = new LatLng(Float.parseFloat(loc[0]), Float.parseFloat(loc[1]));

        //create default and highlight bitmaps for icons
        Bitmap bMapIconDefault = createMapIcon(CurrentStory.getInstance().getYear(), Color.rgb(188, 155, 81), true);
        Bitmap bMapIconHighlight = createMapIcon(CurrentStory.getInstance().getYear(), Color.rgb(255, 255, 255), false);

        //create/add marker to gmap
        mk = gMap.addMarker(new MarkerOptions().position(llStory).title(CurrentStory.getInstance().getTitle()).snippet(CurrentStory.getInstance().getYear()).icon(BitmapDescriptorFactory.fromBitmap(bMapIconHighlight)) );
        //mk.showInfoWindow();
        //save marker and bitmaps into their arrays
        //marMarkers[i] = mk;
        //bitBitmapDefaults[i] = bMapIconDefault;
        //bitBitmapHighlights[i] = bMapIconHighlight;



        //try styling the gmaps (as there could be issues on some version of Android
        try {
        // Customise the styling of the base map using a JSON object defined
        // in a raw resource file.
        boolean success = googleMap.setMapStyle(
        MapStyleOptions.loadRawResourceStyle(
        getContext(), R.raw.style_json));

        if (!success) {
        Log.e("SINGLE_STORY_MAP_VIEW", "Style parsing failed.");
        }
        } catch (Resources.NotFoundException e) {
        Log.e("SINGLE_STORY_MAP_VIEW", "Can't find style. Error: ", e);
        }
        gMap.moveCamera(CameraUpdateFactory.newLatLng(mk.getPosition()));

        }
        //}
        //initialize camera position to first marker that was added

        //set default zoom level
        gMap.moveCamera(CameraUpdateFactory.zoomTo(12));

        //add click listeners
        gMap.setOnMarkerClickListener(this);
        gMap.setOnMapClickListener(this);

        ImageButton btn = (ImageButton) gMapSingleStoryView.findViewById(R.id.back_map);

        /*btn.setText("Back");
        btn.setX(0);
        btn.setY(0);
        btn.setTextColor(Color.BLACK);
        btn.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));*/


        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapViewPager != null){
                    mapViewPager.setCurrentItem(CurrentStory.STORY_VIEW);
                }
                else if(notificationViewPager != null){
                    notificationViewPager.setCurrentItem(0);
                }
                else{
                    timelineMainViewPager.setCurrentItem(CurrentStory.STORY_VIEW);
                }

            }
        });

        //gMapView.addView(btn);

        //gMap.setOnMyLocationButtonClickListener(this);

        TextView textTitle = (TextView) gMapSingleStoryView.findViewById(R.id.storyTitle);

        String year = CurrentStory.getInstance().getYear();
        String title = CurrentStory.getInstance().getTitle();

        // put year and title with different colors
        SpannableStringBuilder builder = new SpannableStringBuilder();

        // style the year
        SpannableString whiteYear= new SpannableString(year);
        whiteYear.setSpan(new ForegroundColorSpan(Color.WHITE), 0, year.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(whiteYear);

        // style the title
        SpannableString blackTitle = new SpannableString(title);
        blackTitle.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(" " + blackTitle);

        // show title with year in white
        textTitle.setText(builder);



}

//I put this in a function since it's used to create both highlight and default icons. could be moved or
//repurposed into a new class or function if needed. Perhaps create a singleton class to hold all the bitmaps?
//we'll see how it affects performance
    private Bitmap createMapIcon(String year, int color, Boolean isDefault) {
        Bitmap icon;
        //get the png graphic based on if we want the default or highlighted icon states
        if (isDefault)
        icon = BitmapFactory.decodeResource(getResources(), R.drawable.map_icon_default);
        else
        icon = BitmapFactory.decodeResource(getResources(), R.drawable.map_icon_white);

        //create bitmap from the png
        //Bitmap bMapScaled = Bitmap.createScaledBitmap(icon, pxToDp(166), pxToDp(265), true);
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        String displayName = display.getName();
        // pixels, dpi
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        int widthPixels = metrics.widthPixels;

        int bitmapWidth = 160;
        if(widthPixels == 1440){
            bitmapWidth = 800;
        }
        else if(widthPixels == 1080){
            bitmapWidth = 600;
        }
        else if(widthPixels == 768) {
            bitmapWidth = 300;
        }
        else if (widthPixels == 480){
            bitmapWidth = 160;
        }
        else{
            bitmapWidth = 160;
        }

        //Bitmap bMapScaled = Bitmap.createScaledBitmap(icon, pxToDp(166), pxToDp(265), true);
        Bitmap bMapScaled = Bitmap.createScaledBitmap(icon, pxToDp(bitmapWidth), pxToDp(bitmapWidth+100), true);
        Bitmap workingBitmap = Bitmap.createBitmap(bMapScaled);
        bMapScaled = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);
        //create canvas so we can add text
        Canvas canvas = new Canvas(bMapScaled);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //color from user
        paint.setColor(color);

        // text size in pixels
        double scale = Resources.getSystem().getDisplayMetrics().density / 1.5;
        float size = 25 * (float) scale;
        paint.setTextSize( size );

        // draw text to the top of the Canvas
        Rect bounds = new Rect();
        paint.getTextBounds(year, 0, year.length(), bounds);
        //set font
        //paint.setTypeface(CustomFont.getInstance().AbrilFont(getBaseContext(), "abril" ));
        paint.setTypeface(CustomFont.getInstance().AbrilFont(getContext(), "abril" ));
        //set position (center top)
        int x = (bMapScaled.getWidth() - bounds.width())/2;
        int y = bounds.height();
        //draw text on icon
        canvas.drawText(year, x, y, paint);

        //return finished bitmap icon with png and year text
        return bMapScaled;
        }

protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build();

        mGoogleApiClient.connect();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.setAlwaysShow(true);
        }



@Override
public void onConnected(@Nullable Bundle bundle) {
        locLocationRequest = new LocationRequest();
        locLocationRequest.setInterval(500);
        locLocationRequest.setFastestInterval(100);
        locLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getContext(),
        android.Manifest.permission.ACCESS_FINE_LOCATION)
        == PackageManager.PERMISSION_GRANTED) {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locLocationRequest, this);
        }


    // get custom location button
    btnMyLocation = (ImageButton) gMapSingleStoryView.findViewById(R.id.my_location);
/*
    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    btnMyLocation.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            getMyLocation();
        }

        private void getMyLocation() {

            if (mLastLocation != null) {
                lat = mLastLocation.getLatitude();
                lng = mLastLocation.getLongitude();

                LatLng latLng = new LatLng(lat, lng);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
                gMap.animateCamera(cameraUpdate);
            }
        }




    });*/

}

@Override
public void onConnectionSuspended(int i) {
        Toast.makeText(getContext(), "Connection suspended, please unsuspend.", Toast.LENGTH_LONG).show();
        }

@Override
public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getContext(), "GPS not enabled, please enable.", Toast.LENGTH_LONG).show();
        }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}


