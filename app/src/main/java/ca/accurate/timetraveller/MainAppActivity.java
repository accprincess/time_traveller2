package ca.accurate.timetraveller;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Random;


public class MainAppActivity extends AppCompatActivity  {

    private MainAppFragment currentFragment;
    private static MainAppViewPagerAdapter adapter;
    private AHBottomNavigationAdapter navigationAdapter;
    private ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();
    private boolean useMenuResource = false;
    private int[] tabColors;
    private Handler handler = new Handler();
    public static String PACKAGE_NAME;

    // UI
    private AHBottomNavigationViewPager viewPager;
    private AHBottomNavigation bottomNavigation;
    private FloatingActionButton floatingActionButton;
    private ArrayList<Integer> pageHistory;
    private boolean fromNotification;
    private int timelinePosition, notificationPosition;
    private int mapPosition;
    private boolean enabledFrench;
    private RelativeLayout showLoad, showSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enabledFrench = getSharedPreferences("shared", getBaseContext().MODE_PRIVATE)
                .getBoolean("language", false);
        if(enabledFrench){
            setLocale("en");
        }
        boolean enabledNotifications = getSharedPreferences("switchNotifications", getBaseContext().MODE_PRIVATE)
                .getBoolean("switchNotifications", false);


        //Navigation Layout
        setContentView(R.layout.main_app_nav_layout);
        //Need to check SDK versions and permissions to Access User's Location/GPS
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        // Array of screens visited
        pageHistory = new ArrayList<Integer>();
        if (getIntent().getExtras() != null) {
            fromNotification = true;
        }

        if(enabledNotifications){
            stopService(new Intent(this, ca.accurate.timetraveller.SensorService.class));
            startService(new Intent(this, ca.accurate.timetraveller.SensorService.class));
        }

        initUI();

        CurrentStory.getInstance().MainStory = this;
        PACKAGE_NAME = getApplicationContext().getPackageName();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    /**
     * Init UI
     */
    private void initUI() {

        boolean isSwitch = getSharedPreferences("isSwitch", getBaseContext().MODE_PRIVATE)
                .getBoolean("isSwitch", false);
        boolean isSwitchN = getSharedPreferences("isSwitchN", getBaseContext().MODE_PRIVATE)
                .getBoolean("isSwitchN", false);
        pageHistory.add(0);
        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        viewPager = (AHBottomNavigationViewPager) findViewById(R.id.view_pager);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.floating_action_button);

        //Display Random Load
        showLoad = (RelativeLayout) findViewById(R.id.random_story);
        //Display Saving
        showSave = (RelativeLayout) findViewById(R.id.save_settings);

        if (useMenuResource) {
            tabColors = getApplicationContext().getResources().getIntArray(R.array.tab_colors);
            navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.bottom_navigation_menu_3);
            navigationAdapter.setupWithBottomNavigation(bottomNavigation, tabColors);
        } else {
            AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.nav_map_icon_default, R.color.color_tab_1);
            AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.nav_timeline_icon_default, R.color.color_tab_2);
            AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.nav_random_icon_default, R.color.color_tab_3);
            AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.nav_settings_icon_default, R.color.color_tab_4);
            bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#000000"));
            bottomNavigation.setAccentColor(Color.parseColor("#d7b466"));
            bottomNavigation.setInactiveColor(Color.parseColor("#828282"));
            bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);

            //Get screen size
            WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

            Display display = wm.getDefaultDisplay();
            String displayName = display.getName();
            // pixels, dpi
            DisplayMetrics metrics = new DisplayMetrics();
            wm.getDefaultDisplay().getMetrics(metrics);
            int widthPixels = metrics.widthPixels;
            //Log.i(TAG, "widthPixels  = " + widthPixels);

            int navFontSize;
            if(widthPixels == 1440){
                navFontSize = 50;
            }
            else if(widthPixels == 1080){
                navFontSize = 30;
            }
            else if(widthPixels == 768) {
                navFontSize = 25;
            }
            else if (widthPixels == 480){
                navFontSize = 20;
            }
            else{
                navFontSize = 15;
            }

            bottomNavigation.setTitleTextSize(navFontSize, navFontSize);

            bottomNavigationItems.add(item1);
            bottomNavigationItems.add(item2);
            bottomNavigationItems.add(item3);
            bottomNavigationItems.add(item4);

            bottomNavigation.addItems(bottomNavigationItems);
        }
        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.manageFloatingActionButtonBehavior(floatingActionButton);
        bottomNavigation.setTranslucentNavigationEnabled(true);

        bottomNavigation.setOnNavigationPositionListener(new AHBottomNavigation.OnNavigationPositionListener() {
            @Override
            public void onPositionChange(int y) {
                bottomNavigation.refresh();
            }
        });


        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {

                int lastPosition = pageHistory.get(pageHistory.size() -1);
                if(lastPosition != position){
                    pageHistory.add(position);
                }

                if (currentFragment == null) {
                    currentFragment = adapter.getCurrentFragment();
                }

                if (currentFragment != null) {
                    currentFragment.willBeHidden();
                }

                if (position == 2) {
                    final ImageView rnd_icon = (ImageView) showLoad.findViewById(R.id.random_icon);
                    RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    anim.setInterpolator(new LinearInterpolator());
                    anim.setRepeatCount(Animation.INFINITE);
                    anim.setDuration(2000);
                    showLoad.setVisibility(View.VISIBLE);
                    rnd_icon.startAnimation(anim);
                    new Handler().postDelayed(Variables.getInstance().randomDelay = new Runnable(){
                        @Override
                        public void run() {
                            Random rand = new Random();
                            int  n = rand.nextInt(CurrentStory.getInstance().getStoryCount()) + 1;
                            CurrentStory.getInstance().setCurrentStory(Integer.toString(n),getApplicationContext());
                            refreshBottomNav(1);
                            MainAppActivity.GoToTimelineView();
                            showLoad.setVisibility(View.GONE);
                            rnd_icon.setAnimation(null);
                        }
                    }, 2000);

                }else{
                    viewPager.setCurrentItem(position, false);
                    currentFragment = adapter.getCurrentFragment();
                }
                if (position != 1) {
                    MainAppActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                else {
                    MainAppActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                }
                return true;
            }
        });

        viewPager.setOffscreenPageLimit(4);
        adapter = new MainAppViewPagerAdapter(getSupportFragmentManager(), getBaseContext() );
        viewPager.setAdapter(adapter);

        currentFragment = adapter.getCurrentFragment();

        if(fromNotification){
            int id = getIntent().getIntExtra("storyId", 1);
            String title = CurrentStory.getInstance().getSpecificTitle(id);
            int index2 = CurrentStory.getInstance().FindStoryByTitle(title);
            CurrentStory.getInstance().setCurrentStory(Integer.toString(index2 + 1), getBaseContext());
            bottomNavigation.setSelected(true);
            refreshBottomNav(1);
            viewPager.setCurrentItem(4);
            //System.out.println("From Notification");
        }

        if(isSwitch || isSwitchN){
            showSave.setVisibility(View.VISIBLE);
            refreshBottomNav(3);
            new Handler().postDelayed(Variables.getInstance().randomDelay = new Runnable(){
                @Override
                public void run() {
                    showSave.setVisibility(View.GONE);
                }
            }, 2000);
        }

        adapter.SetBottomNav(bottomNavigation);

    }
    private void refreshBottomNav(int currentItem) {
        bottomNavigation.setCurrentItem(currentItem);
        bottomNavigation.refresh();
    }

    public static void GoToTimelineView() {
        adapter.getCurrentFragment().refresh(1);
    }

    /**
     * Show or hide the bottom navigation with animation
     */
    public void showOrHideBottomNavigation(boolean show) {
        if (show) {
            bottomNavigation.restoreBottomNavigation(true);
        } else {
            bottomNavigation.hideBottomNavigation(true);
        }
    }

    /**
     * Show or hide selected item background
     */
    public void updateSelectedBackgroundVisibility(boolean isVisible) {
        bottomNavigation.setSelectedBackgroundVisible(isVisible);
    }

    /**
     * Show or hide selected item background
     */
    public void setForceTitleHide(boolean forceTitleHide) {
        AHBottomNavigation.TitleState state = forceTitleHide ? AHBottomNavigation.TitleState.ALWAYS_HIDE : AHBottomNavigation.TitleState.ALWAYS_SHOW;
        bottomNavigation.setTitleState(state);
    }

    /**
     * Reload activity
     */
    public void reload() {
        startActivity(new Intent(this, MainAppActivity.class));
        finish();
    }


    @Override
    public void onBackPressed() {

        currentFragment = adapter.getCurrentFragment();
        if(pageHistory.size() != 0) {

            //Map View
            if (currentFragment.getIndex() == 0) {

                mapPosition = currentFragment.getMapViewPosition();
                if (mapPosition == 3 || mapPosition == 4 || mapPosition == 2) {
                    currentFragment.getMapViewPager().setCurrentItem(1);
                }else if(mapPosition == 1){
                    currentFragment.getMapViewPager().setCurrentItem(0);
                }
                else{
                    if(pageHistory.size() == 1){
                        viewPager.setCurrentItem(0);
                    }
                    else{
                        backFragments();
                    }
                }

            }
            //Timeline View
            else if (currentFragment.getIndex() == 1) {

                timelinePosition = currentFragment.getTimelineViewPosition();
                if (timelinePosition == 3 || timelinePosition == 4 || timelinePosition == 2) {
                    currentFragment.getTimelineViewPager().setCurrentItem(1);
                }else if(timelinePosition == 1){
                    currentFragment.getTimelineViewPager().setCurrentItem(0);
                }
                else{
                    backFragments();
                }

            }
            //Settings View
            else if (currentFragment.getIndex() == 3) {
                int settingsPos = currentFragment.getSettingsViewPosition();

                if (settingsPos == 2 || settingsPos == 1) {
                    currentFragment.getSettingsViewPager().setCurrentItem(0);
                }
                else {
                    backFragments();
                }
            }
            //Notification View
            else if (currentFragment.getIndex() == 4) {

                notificationPosition = currentFragment.getNotificationPosition();
                if (notificationPosition == 1 || notificationPosition == 2 || notificationPosition == 3) {
                    currentFragment.getNotificationViewPager().setCurrentItem(0);
                }
                else{
                    backFragments();
                }

            }
        }else{
            viewPager.setCurrentItem(0);
        }
    }

    private void backFragments(){
        bottomNavigation.setCurrentItem(pageHistory.get(pageHistory.size() - 2));
        Collections.reverse(pageHistory);
        for (int i = 0; i <= 1; i++) {
            pageHistory.remove(0);
        }
        Collections.reverse(pageHistory);
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * Return the number of items in the bottom navigation
     */
    public int getBottomNavigationNbItems() {
        return bottomNavigation.getItemsCount();
    }

    public void setLocale(String lang) {

        String langPref = "Language";
        SharedPreferences setPrefs = getBaseContext().getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = setPrefs.edit();
        editor.putBoolean(langPref, false);
        editor.apply();

        Locale myLocale = new Locale(lang);
        Configuration config = new Configuration();
        config.locale = myLocale;
        getResources().updateConfiguration(config, null);

        SharedPreferences prefs = getBaseContext().getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        //String lan = prefs.getString("language", "");

    }

}
