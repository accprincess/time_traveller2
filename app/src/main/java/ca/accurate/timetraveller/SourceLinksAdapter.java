package ca.accurate.timetraveller;

/**
 * Created by C on 2017-02-28.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class SourceLinksAdapter extends RecyclerView.Adapter<SourceLinksAdapter.NumberViewHolder> {

    private static final String TAG = SourceLinksAdapter.class.getSimpleName();

    private ArrayList<String> sourceLinks;
    final private ListItemClickListener mOnClickListener;
    private int selectedPosition=0;

    public interface ListItemClickListener {
        void onListItemClick(int clickedItemIndex);
    }

    public SourceLinksAdapter(ArrayList<String> numberOfItems, ListItemClickListener listener) {
        sourceLinks = numberOfItems;
        mOnClickListener = listener;
    }

    @Override
    public NumberViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.story_source_links;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        NumberViewHolder viewHolder = new NumberViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(NumberViewHolder holder, int position) {
        holder.textStoryLinks.setText(sourceLinks.get(position));
//        holder.textStoryLinks.setText(Html.fromHtml(
//                "<a href=\""+sourceLinks.get(position)+"\">"+sourceLinks.get(position)+"</a> "));
//        holder.textStoryLinks.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public int getItemCount() {
        return sourceLinks.size();
    }


    class NumberViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
        TextView textStoryLinks;

        public NumberViewHolder(View itemView) {
            super(itemView);
            textStoryLinks = (TextView) itemView.findViewById(R.id.story_source_links);
            itemView.setOnClickListener(this);
        }

        void bind(int listIndex) {
            textStoryLinks.setText(String.valueOf(listIndex));
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
//            currentYear = yearsList.get(clickedPosition);

            mOnClickListener.onListItemClick(clickedPosition);
            selectedPosition=clickedPosition;
            notifyDataSetChanged();

        }
    }
}
