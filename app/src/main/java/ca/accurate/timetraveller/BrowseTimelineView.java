package ca.accurate.timetraveller;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashSet;

/**
 * Created by P on 2017-01-24.
 */

public class BrowseTimelineView extends FrameLayout implements DecadeScrollAdapter.ListItemClickListener {
    private LayoutInflater layoutInflater;
    private ViewGroup viewGroup;
    private FragmentManager fragmentManager;
    private Context conContext;
    private View browseTimelineView;
    private TimelineMainViewPager timelineMainViewPager;
    private DecadeScrollAdapter mAdapter;
    private RecyclerView mNumbersList;
    private int[] layouts;
    private int iCount = CurrentStory.getInstance().getStoryCount();
    private ViewPager storySnippetViewPager;
    private StorySnippetPagerAdapter storySnippetAdapter;
    private EditText enterYear;
    private TextView timelineYear, timelineTitle;
    private String year, title;
    private SpannableStringBuilder builder = new SpannableStringBuilder();;
    private ForegroundColorSpan whiteColor = new ForegroundColorSpan(Color.WHITE);
    private ForegroundColorSpan blackColor = new ForegroundColorSpan(Color.BLACK);

    public BrowseTimelineView(Context context, ViewGroup container, LayoutInflater inflater, FragmentManager fManager, TimelineMainViewPager tmMainVP) {
        super(context);
        timelineMainViewPager = tmMainVP;
        conContext = context;
        viewGroup = container;
        layoutInflater = inflater;
        fragmentManager = fManager;

        browseTimelineView = layoutInflater.inflate(R.layout.fragment_browse_timeline_view, container, false);

        //container.addView(browseTimelineView);

        Button startBtn = (Button) browseTimelineView.findViewById(R.id.startAt_btn);
        startBtn.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CurrentStory.getInstance().setCurrentStory("1", conContext);
                timelineMainViewPager.setCurrentItem(CurrentStory.STORY_VIEW);
                return false;
            }
        });

        enterYear = (EditText) browseTimelineView.findViewById(R.id.year_input_txt);
        //enterYear.setText("0");
        ImageButton goBtn = (ImageButton) browseTimelineView.findViewById(R.id.goToYear_btn);

        mNumbersList = (RecyclerView)browseTimelineView.findViewById(R.id.rv_numbers);
        LinkedHashSet<String> uniqueDecade = new LinkedHashSet<String>();

        String firstYear = CurrentStory.getInstance().getSpecificYear(0);
        int fy = Integer.parseInt(firstYear);
        int roundFy = Math.round(fy/10) * 10;

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
//        layouts = new int[5];
//        for (int i = 0; i < 5; i++) {
//            layouts[i] = R.layout.tutorial_layout;
//        }

        for (int i = roundFy; i < 2020; i+=10) {
            mNumbersList.setLayoutManager(layoutManager);
            mNumbersList.setHasFixedSize(true);

            for(int x = 0; x < iCount; x++){
                String currentStory = CurrentStory.getInstance().getSpecificYear(x);
                int roundCy = Math.round(Integer.parseInt(currentStory)/10) * 10;
                if (roundCy == i) {
                    String yearWithValue =  Integer.toString(i);
                    uniqueDecade.add(yearWithValue);
                }
            }
        }
        final ArrayList<String> years = new ArrayList<String>(uniqueDecade);
        mAdapter = new DecadeScrollAdapter(years, this);
        mNumbersList.setAdapter(mAdapter);

        storySnippetViewPager = (ViewPager) browseTimelineView.findViewById(R.id.story_snippet_slider);
        storySnippetAdapter = new StorySnippetPagerAdapter(conContext, layoutInflater, fragmentManager, timelineMainViewPager, viewGroup);
        storySnippetViewPager.setAdapter(storySnippetAdapter);

        //timelineYear = (TextView) browseTimelineView.findViewById(R.id.decade_year);
        timelineTitle = (TextView) browseTimelineView.findViewById(R.id.decade_title);
        ImageButton prevBtn = (ImageButton) browseTimelineView.findViewById(R.id.prev_button);
        ImageButton nextBtn = (ImageButton) browseTimelineView.findViewById(R.id.next_button);

        //storySnippetViewPager.setCurrentItem(0);

        viewpagerTitle(0);

        goBtn.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Toast mToast;
                String yearEntered = enterYear.getText().toString();

                if(yearEntered.isEmpty() || yearEntered.length() == 0 || yearEntered.equals("") || yearEntered == null)
                {
                    String toastMessage = "Enter a year";
                    mToast = Toast.makeText(getContext(), toastMessage, Toast.LENGTH_SHORT);
                    mToast.show();
                }
                else{
                    int yearEnteredInt = Integer.parseInt(yearEntered);
                    for (int x = 0; x < iCount; x++) {
                        int storyYear = Integer.parseInt(CurrentStory.getInstance().getSpecificYear(x));
                        String last = years.get(years.size() - 1);
                        if (storyYear == yearEnteredInt) {
                            CurrentStory.getInstance().setCurrentStory(Integer.toString(x + 1), conContext);
                            timelineMainViewPager.setCurrentItem(CurrentStory.STORY_VIEW);
//                              InputMethodManager mgr = (InputMethodManager) CurrentStory.getInstance().MainStory.getSystemService(Context.INPUT_METHOD_SERVICE);
//                              mgr.hideSoftInputFromWindow(enterYear.getWindowToken(), 0);
                            break;

                        } else if (storyYear >= yearEnteredInt) {
                            String toastMessage = "Not Found";
                            mToast = Toast.makeText(getContext(), toastMessage, Toast.LENGTH_SHORT);
                            mToast.show();
                            break;

                        } else if (storyYear >= Integer.parseInt(last)) {
                            String toastMessage = "Not Found";
                            mToast = Toast.makeText(getContext(), toastMessage, Toast.LENGTH_SHORT);
                            mToast.show();
                            break;
                        }

                    }
                }

                return false;

            }
        });

//        enterYear.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean focused) {
//                InputMethodManager keyboard = (InputMethodManager) CurrentStory.getInstance().MainStory.getSystemService(Context.INPUT_METHOD_SERVICE);
//                if (focused)
//                    keyboard.showSoftInput(enterYear, 0);
//                else
//                    keyboard.hideSoftInputFromWindow(enterYear.getWindowToken(), 0);
//                    Toast mToast;
//                    if (enterYear.getText().toString() != (String) " ") {
//                        int yearEntered = Integer.parseInt(enterYear.getText().toString());
//                        if (yearEntered == 0) {
//                            for (int x = 0; x < iCount; x++) {
//                                int storyYear = Integer.parseInt(CurrentStory.getInstance().getSpecificYear(x));
//                                String last = years.get(years.size() - 1);
//                                if (storyYear == yearEntered) {
//                                    CurrentStory.getInstance().setCurrentStory(Integer.toString(x + 1), conContext);
//                                    timelineMainViewPager.setCurrentItem(CurrentStory.STORY_VIEW);
//                                    InputMethodManager mgr = (InputMethodManager) CurrentStory.getInstance().MainStory.getSystemService(Context.INPUT_METHOD_SERVICE);
//                                    mgr.hideSoftInputFromWindow(enterYear.getWindowToken(), 0);
//
//                                    break;
//                                } else if (storyYear >= yearEntered) {
//                                    String toastMessage = "Not Found";
//                                    mToast = Toast.makeText(getContext(), toastMessage, Toast.LENGTH_SHORT);
//                                    mToast.show();
//                                    break;
//                                } else if (storyYear >= Integer.parseInt(last)) {
//                                    String toastMessage = "Not Found";
//                                    mToast = Toast.makeText(getContext(), toastMessage, Toast.LENGTH_SHORT);
//                                    mToast.show();
//                                    break;
//                                }
//
//                            }
//                        }
//
//                    }
//                }
//        });

        prevBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItemPrev(0);
                storySnippetViewPager.setCurrentItem(current);
            }
        });

        nextBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(0);
                storySnippetViewPager.setCurrentItem(current);
            }
        });

        storySnippetViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                Log.d("TAG", "OnPageScrolled");
            }

            @Override
            public void onPageSelected(int position) {
                viewpagerTitle(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
//                Log.d("TAG", "onPageScrollStateChanged");
            }
        });

        this.addView(browseTimelineView);
    }
    private void viewpagerTitle(int position){
        year = CurrentStory.getInstance().getSpecificYear(position);
        title = CurrentStory.getInstance().getSpecificTitle(position);

        // style the year
        SpannableString whiteYear = new SpannableString(year);
        whiteYear.setSpan(whiteColor, 0, year.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(whiteYear);
        whiteYear = null;
        // style the title
        SpannableString blackTitle = new SpannableString(title);
        blackTitle.setSpan(blackColor, 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(" " + blackTitle);
        blackTitle = null;
        // show title with year in white
        timelineTitle.setText(builder);
        builder.clear();
        //builder = null;
        whiteYear = null;
        blackTitle = null;
    }

    private int getItemPrev(int i) {
        return storySnippetViewPager.getCurrentItem() - 1;
    }
    private int getItem(int i) {
        return storySnippetViewPager.getCurrentItem() + 1;
    }

    public View GetView() {
        return browseTimelineView;
    }

    public void onListItemClick(int clickedItemIndex) {


        for (int x = 0; x < iCount; x++) {
            int storyYear = Integer.parseInt(CurrentStory.getInstance().getSpecificYear(x));
            int mAdapterYear = Integer.parseInt(mAdapter.GetCurrentYear());
            if ( storyYear >= mAdapterYear )
            {
                storySnippetViewPager.setCurrentItem(x);
                break;
            }
        }
        //do stuff with targetstory and load it up below in the recyclerview
    }
}
