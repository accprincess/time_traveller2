package ca.accurate.timetraveller;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by Spencers on 31/01/2017.
 */

public class SettingsHelpView extends FrameLayout {
    //private LayoutInflater layoutInflater;
    private ViewGroup viewGroup;
    private FragmentManager fragmentManager;
    private Context conContext;
    private View settingsHelpView;
    private SettingsViewPager settingsViewPager;


    public SettingsHelpView(Context context, ViewGroup container, LayoutInflater inflater, FragmentManager fManager, SettingsViewPager setViewPager) {
        super(context);
        settingsViewPager = setViewPager;
        conContext = context;
        viewGroup = container;
        //layoutInflater = inflater;
        fragmentManager = fManager;
        LayoutInflater layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        settingsHelpView = layoutInflater.inflate(R.layout.settings_help_view, container, false);
        ImageView imageView = (ImageView) settingsHelpView.findViewById(R.id.main_image);
        Glide.with(context).load(getResources()
                .getIdentifier("main_logo", "drawable", context.getPackageName()))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView);

        this.addView(settingsHelpView);
    }

    public void ResetData() {

        TextView setting_help_description = (TextView) settingsHelpView.findViewById(R.id.setting_help_description);
        ImageButton backBtn = (ImageButton) settingsHelpView.findViewById(R.id.back_description);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Spinnaker-Regular.ttf");
        setting_help_description.setTypeface(tf);

        if(settingsHelpView.getParent()!=null)
            ((ViewGroup)settingsHelpView.getParent()).removeView(settingsHelpView); // <- fix
        viewGroup.addView(settingsHelpView);
        //container.addView(v);

        backBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsViewPager.setCurrentItem(0);

            }
        });

    }

    public View GetView() {
        return settingsHelpView;
    }
}
