package ca.accurate.timetraveller;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.BitmapFactory;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;


//import static ca.accurate.timetraveller.R.id.videoSurface;
//import static ca.accurate.timetraveller.R.id.videoSurfaceContainer;

/**
 * Created by P on 2017-01-18.
 */

public class SingleStory extends FrameLayout{ //implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, VideoControllerView.MediaPlayerControl {
    private Context conContext;
    private View singleStoryView;
    private TimelineMainViewPager timelineSlider;
    private ViewGroup viewGroup;
    private MapViewPager mapViewPager;
    private NotificationViewPager nViewPager;

    public SingleStory(Context cContext, ViewGroup container, TimelineMainViewPager time, MapViewPager mVPager, NotificationViewPager notificationViewPager) {
        super(cContext);
        timelineSlider = time;
        mapViewPager = mVPager;
        nViewPager = notificationViewPager;
        conContext = cContext;
        viewGroup = container;

        LayoutInflater layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        singleStoryView = layoutInflater.inflate(R.layout.single_story, container, false);
        //Button backBtn = (Button) singleStoryView.findViewById(R.id.back);
        ImageButton backBtn = (ImageButton) singleStoryView.findViewById(R.id.back_single_story);
        //timelineStoryMain = (RecyclerView) singleStoryView.findViewById(R.id.recycler_timelinestory);

        // tStoryScrollAdapter = new TimelineStoryScrollAdapter(0, this);
        //mNumbersList.setAdapter(mAdapter);
        //Button backBtn = (Button) singleStoryView.findViewById(R.id.back_btn_single_story);

//        if (timelineSlider != null || mapViewPager != null || nViewPager != null){
//            backBtn.setVisibility(INVISIBLE);
//        }
        backBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapViewPager != null){
                    mapViewPager.setCurrentItem(CurrentStory.getInstance().TIMELINE_VIEW);
                }
                else if(nViewPager != null){
                    nViewPager.setCurrentItem(0);
                }
                else if(timelineSlider != null){
                    timelineSlider.setCurrentItem(CurrentStory.getInstance().TIMELINE_VIEW);
                }
                else{
                    //
                }
            }
        });

        Button descBtn = (Button) singleStoryView.findViewById(R.id.description_button);
        descBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapViewPager != null){
                    mapViewPager.setCurrentItem(CurrentStory.getInstance().DESCRIPTION_VIEW);
                }
                else if(nViewPager != null){
                    nViewPager.setCurrentItem(1);
                }
                else{
                    timelineSlider.setCurrentItem(CurrentStory.getInstance().DESCRIPTION_VIEW);
                }
            }
        });

        Button mapBtn = (Button) singleStoryView.findViewById(R.id.map_button);
        mapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapViewPager != null){
                    mapViewPager.setCurrentItem(CurrentStory.getInstance().MAP_VIEW);
                }
                else if(nViewPager != null){
                    nViewPager.setCurrentItem(3);
                }
                else{
                    timelineSlider.setCurrentItem(CurrentStory.getInstance().MAP_VIEW);
                }
            }
        });

        Button srcBtn = (Button) singleStoryView.findViewById(R.id.source_button);
        srcBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapViewPager != null){
                    mapViewPager.setCurrentItem(CurrentStory.getInstance().SOURCE_VIEW);
                }
                else if(nViewPager != null){
                    nViewPager.setCurrentItem(2);
                }
                else{
                    timelineSlider.setCurrentItem(CurrentStory.getInstance().SOURCE_VIEW);
                }
            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapViewPager != null){
                    mapViewPager.setCurrentItem(CurrentStory.getInstance().TIMELINE_VIEW);
                }else if(timelineSlider != null){
                    timelineSlider.setCurrentItem(CurrentStory.getInstance().TIMELINE_VIEW);
                }
            }
        });


        //container.addView(v);
        this.addView(singleStoryView);
        //return v;

    }

    public View GetView() {
        return singleStoryView;
    }

    public void GoToTimelineView() {
        timelineSlider.setCurrentItem(CurrentStory.TIMELINE_VIEW);
    }

    public void ResetData() {
        TextView text1 = (TextView) singleStoryView.findViewById(R.id.year);
        TextView text2 = (TextView) singleStoryView.findViewById(R.id.storyTitle);
        ImageView img= (ImageView) singleStoryView.findViewById(R.id.story_featured_image);

        String imgs[] = CurrentStory.getInstance().getImages();
        int img_id = conContext.getResources().getIdentifier(imgs[0], "drawable", conContext.getPackageName());
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = 4;
//        img.setImageBitmap(BitmapFactory.decodeResource(conContext.getResources(), img_id, opts));

        Glide.with(conContext).load(img_id)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(img);
        //img.setImageResource(img_id);
        //Button backBtn = (Button) singleStoryView.findViewById(R.id.back);
        text1.setText(CurrentStory.getInstance().getYear());
        text2.setText(CurrentStory.getInstance().getTitle());

    }
    public void GoToStoryView() {
        timelineSlider.setCurrentItem(CurrentStory.STORY_VIEW);
    }

}
