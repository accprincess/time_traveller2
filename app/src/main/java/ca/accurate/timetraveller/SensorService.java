package ca.accurate.timetraveller;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by P on 30/01/2017.
 */
public class SensorService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    final static String GROUP_EVENTS = "events";
    IBinder mBinder = new SensorService.LocalBinder();
    private static final int LOCATION_CHECK_INTERVAL = 1000;
    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;
    private LocationManager locMan;
    // Flag that indicates if a request is underway.
    private boolean mInProgress;
    Location gpslocation = null;
    private Boolean servicesAvailable = false;
    private Context conContext;
    private Resources res;// = getBaseContext().getResources();
    private String[] Pins;// = res.getStringArray(R.array.locations);
    private Boolean[] read;// = new Boolean[Pins.length];
    List<String> matchingProviders;
    Location location;


    public int counter=0;
    public SensorService(Context applicationContext) {
        super();
        Log.i("HERE", "here I am!");
        conContext = applicationContext;
        //conContext = getBaseContext();
    }

    public class LocalBinder extends Binder {
        public SensorService getServerInstance() {
            return SensorService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();


        mInProgress = false;
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(Constants.UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(Constants.FASTEST_INTERVAL);

        servicesAvailable = servicesConnected();
        res = getBaseContext().getResources();
        Pins = res.getStringArray(R.array.locations);
        read = new Boolean[Pins.length];
        Arrays.fill(read, Boolean.FALSE);
        conContext = getBaseContext();
        if (CurrentStory.getInstance().getCurrentStoryID() == -1 ) {
            CurrentStory.getInstance().setCurrentStory("1", getBaseContext());
        } else {
            CurrentStory.getInstance().setContext(conContext);
        }
        //PrincessMay22,18
        //android.os.Debug.waitForDebugger();
        /*
         * Create a new location client, using the enclosing class to
         * handle callbacks.
         */
        setUpLocationClientIfNeeded();


    }

    private boolean servicesConnected() {

        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {

            return true;
        } else {

            return false;
        }
    }

    private void setUpLocationClientIfNeeded()
    {
        if(mGoogleApiClient == null)
            buildGoogleApiClient();
    }

    public SensorService() {
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        Intent broadcastIntent = new Intent("uk.ac.shef.oak.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);
        stoptimertask();
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime=0;
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every x second
        timer.schedule(timerTask, LOCATION_CHECK_INTERVAL, LOCATION_CHECK_INTERVAL); //
    }

    private void newObtainLocation() {
        //Location bestResult = null;
        //float bestAccuracy = Float.MAX_VALUE;
        //float minAccuracy = 100;
        //float maxAge = 100;
        //long bestAge = Long.MIN_VALUE;
        matchingProviders = locMan.getAllProviders();
        // to detect phone screen is off or locked
        //PowerManager pm = (PowerManager) conContext.getSystemService(Context.POWER_SERVICE);
        //KeyguardManager km = (KeyguardManager) conContext.getSystemService(Context.KEYGUARD_SERVICE);
        //String provider = LocationManager.GPS_PROVIDER;

        for (String provider : matchingProviders) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                location = locMan.getLastKnownLocation(provider);
                if (location != null) {

                    for (int i = 0; i < Pins.length; i++) {

                        String title = CurrentStory.getInstance().getSpecificTitle(i);
                        String year = CurrentStory.getInstance().getSpecificYear(i);
                        int id = i;

                        //pin location
                        String[] loc = CurrentStory.getInstance().getSpecificLocation(i);
                        Float lat = Float.parseFloat(loc[0]);
                        Float lng = Float.parseFloat(loc[1]);
                        Location l = new Location("pin location");
                        l.setLatitude(lat);
                        l.setLongitude(lng);


                        // calculate distance between user and pins
                        float distance = location.distanceTo(l);
                        int d = (int) Math.round(distance);
                        // System.out.println(d);
                        // send notification of the closest pins only
                        if (d < 1000) { // distance is in Meters

                            if (read[i].equals(false)) {

//                            if (pm.isScreenOn()) {
//
//                                if( km.inKeyguardRestrictedInputMode()) {
//                                    //it is locked
//                                    //sendNotification(title,year, read[i], id,d,cContext);
//                                    System.out.println("screen is LOCKED");
//
//                                }
//
//                                System.out.println("screen is ON");
//                            } else {
//                                //sendNotification(title,year, read[i], id,d,cContext);
//                            }

                                sendNotification(title, year, read[i], id, d, conContext);
                                read[i] = true;
                                //break;

                            }

                        }

                    }
                }

            }
        }


    }

    private void sendNotification(String t, String y, Boolean r, int id, int d, Context c) {
        String title = t;
        String year = y;
        Boolean read = r;
        int ID = id;
        int unread_notif = 0;
        int distance = d;
        //cContext = c;
        int color = ContextCompat.getColor(getBaseContext(), R.color.notificationAccent);

        NotificationManager manager = (NotificationManager) getBaseContext().getSystemService(getBaseContext().NOTIFICATION_SERVICE);

        //Intent notificationIntent = new Intent(getBaseContext(), MainAppActivity.class);
        Intent notificationIntent = new Intent(getBaseContext(), MainAppActivity.class);
        notificationIntent.putExtra("storyId", id);
        notificationIntent.putExtra("fromNotification", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Notification builder = new NotificationCompat.Builder(getBaseContext())
                .setLargeIcon(BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.notification_icon))
                .setSmallIcon(R.drawable.nav_timeline_icon_highlighted) // needs the small icon to work properly & needs to work as white
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.DEFAULT_ALL)
                .setContentTitle("Event is Nearby!")
                .setContentIntent(pendingIntent)
                .setColor(color)
                .setOngoing(false)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setGroup(GROUP_EVENTS)
                .setContentText("It's " + distance + "m close to you")
                .setGroupSummary(true)
                .addAction(R.drawable.nav_timeline_icon_default, title, pendingIntent)
                .build();

        boolean enabledNotifications = getSharedPreferences("switchNotifications", getBaseContext().MODE_PRIVATE)
                .getBoolean("switchNotifications", false);

        if(enabledNotifications){
            manager.notify(ID, builder);
        }

        /*
        NOT WORKING: trying to make it so when new notifications appears, it show them with unread ones

        unread_notif++;

        if (unread_notif>1) {
            Notification summaryNotification = new NotificationCompat.Builder(cContext)
                    .setContentTitle("Nearby Events")
                    .setLargeIcon(BitmapFactory.decodeResource(cContext.getResources(), R.drawable.notification_icon))
                    .setSmallIcon(R.drawable.nav_timeline_icon_highlighted) // needs the small icon to work properly & needs to work as white
                    .setStyle(new NotificationCompat.InboxStyle()
                            .addLine("Details about your first notification")
                            .addLine("Details about your second notification")
                            .setBigContentTitle(Integer.toString(unread_notif)+" new notifications")
                            .setSummaryText("More details in app"))
                    .setGroup(GROUP_EVENTS)
                    .setGroupSummary(true)
                    .build();

            manager.notify(id++, summaryNotification);
        }*/

    }


    private void obtainLocation(){
        if(locMan==null)
            locMan = (LocationManager) getSystemService(LOCATION_SERVICE);

        if( locMan.isProviderEnabled(LocationManager.GPS_PROVIDER) || locMan.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ){
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                gpslocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                gpslocation = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                gpslocation = new Location(LocationManager.GPS_PROVIDER);


            }
            //gpslocation = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        }
    }


    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                //Log.i("in timer", "in timer ++++  "+ (counter++));
                obtainLocation();
                newObtainLocation();
            }
        };
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    /*
     * Create a new location client, using the enclosing class to
     * handle callbacks.
     */
    protected synchronized void buildGoogleApiClient() {
        this.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Request location updates using static settings
            Intent intent = new Intent(this, LocationReceiver.class);
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient,
                    mLocationRequest, this); // This is the changed line.
            //appendLog(DateFormat.getDateTimeInstance().format(new Date()) + ": Connected", Constants.LOG_FILE);
        }
    }

    /*
 * Called by Location Services if the connection to the
 * location client drops because of an error.
 */
    @Override
    public void onConnectionSuspended(int i) {
        // Turn off the request flag
        mInProgress = false;
        // Destroy the current location client
        mGoogleApiClient = null;
        // Display the connection status
        // Toast.makeText(this, DateFormat.getDateTimeInstance().format(new Date()) + ": Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
        //appendLog(DateFormat.getDateTimeInstance().format(new Date()) + ": Disconnected", Constants.LOG_FILE);
    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mInProgress = false;

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {

            // If no resolution is available, display an error dialog
        } else {

        }
    }

    // Define the callback method that receives location updates
    @Override
    public void onLocationChanged(Location location) {
        // Report to the UI that the location was updated
        String msg = Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Log.d("debug", msg);
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        //appendLog(DateFormat.getDateTimeInstance().format(new Date()) + ":" + msg, Constants.LOCATION_FILE);
    }
}
