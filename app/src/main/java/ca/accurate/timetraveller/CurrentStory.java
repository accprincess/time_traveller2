package ca.accurate.timetraveller;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.Log;

/**
 * Created by P on 2016-12-20.
 */
public class CurrentStory {
    public static final int TIMELINE_VIEW = 0;
    public static final int STORY_VIEW = 1;
    public static final int DESCRIPTION_VIEW = 2;
    public static final int SOURCE_VIEW = 3;
    public static final int MAP_VIEW = 4;


    public Activity MainStory;
    private int iStoryNum = -1;
    private String sStoryNum;
    private String sYear;
    private String sTitle;
    private String[] sLocation;
    private String[] sImages;
    private String[] sImageCaptions;
    private String[] sImageAlts;
    private String[] sLinks;
    private String[] sVideos;
    private String[] sVideoThumbs;
    private String sContent;
    private int iStoryCount;
    private int iLang = 0;
    private Context conContext;
    private Resources resResources;

    private static CurrentStory ourInstance = new CurrentStory();

    public static CurrentStory getInstance() {
        return ourInstance;
    }

    private CurrentStory() {

    }

    public void setCurrentStory(String sCurrentStory, Context cContext) {
        sStoryNum = sCurrentStory;
        iLang = Language.getInstance().getLanguage();
        iStoryNum = Integer.parseInt(sStoryNum) - 1;
        conContext = cContext;
        resResources = conContext.getResources();



        //main stories content
        TypedArray ta = resResources.obtainTypedArray(R.array.stories);
        iStoryCount = ta.length();
        int resID = ta.getResourceId(iStoryNum, 0);
        if (resID > 0) {
            String[] main_arr = resResources.getStringArray(resID);
            sContent = main_arr[iLang];
        }



        //title
        ta = resResources.obtainTypedArray(R.array.titles);
        resID = ta.getResourceId(iStoryNum, 0);
        if (resID > 0) {
            sTitle = resResources.getStringArray(resID)[iLang];
        }

        //locations
        ta = resResources.obtainTypedArray(R.array.locations);
        resID = ta.getResourceId(iStoryNum, 0);
        if (resID > 0) {
            sLocation = resResources.getStringArray(resID);
        }

        //videos
        ta = resResources.obtainTypedArray(R.array.videos);
        resID = ta.getResourceId(iStoryNum, 0);
        if (resID > 0) {
            sVideos = resResources.getStringArray(resID);
        }

        //video-thumbs
//        ta = resResources.obtainTypedArray(R.array.video_thumbs);
//        resID = ta.getResourceId(iStoryNum, 0);
//        if (resID > 0) {
//            sVideoThumbs = resResources.getStringArray(resID);
//        }

        //images
        ta = resResources.obtainTypedArray(R.array.images);
        resID = ta.getResourceId(iStoryNum, 0);
        if (resID > 0) {
            sImages = resResources.getStringArray(resID);
        }

        //image-captions
        ta = resResources.obtainTypedArray(R.array.images_captions);
        resID = ta.getResourceId(iStoryNum, 0);
        if (resID > 0) {
            sImageCaptions = resResources.getStringArray(resID);
        }

        //image-alts
        ta = resResources.obtainTypedArray(R.array.images_alts);
        resID = ta.getResourceId(iStoryNum, 0);
        if (resID > 0) {
            sImageAlts = resResources.getStringArray(resID);
        }

        //links
        ta = resResources.obtainTypedArray(R.array.links);
        resID = ta.getResourceId(iStoryNum, 0);
        if (resID > 0) {
            sLinks = resResources.getStringArray(resID);
        }

        //year
        ta = resResources.obtainTypedArray(R.array.years);
        resID = ta.getResourceId(iStoryNum, 0);//.getResourceId(iStoryNum, 0);
        if (resID > 0) {
            sYear = resResources.getStringArray(resID)[0];
        }

}

    public void setContext(Context c) {
        conContext = c;
    }

    public int getCurrentStoryID() {
        return iStoryNum;
    }

    public String getTitle() {
        return sTitle;
    }

    public String getContent() {
        return sContent;
    }

    public String getYear() {
        return sYear;
    }

    public String[] getLocation() {
        return sLocation;
    }

    public String[] getImages() {
        return sImages;
    }

    public String[] getImageCaptions() {
        return sImageCaptions;
    }

    public String[] getImageAlts() {
        return sImageAlts;
    }

    public String[] getLinks() {
        return sLinks;
    }

    public String[] getVideos() {
        return sVideos;
    }
    
    public String[] getVideoThumbs() {
        return sVideoThumbs;
    }

    public int getStoryCount() { return iStoryCount; }

    public String[] getSpecificLocation(int storyNum) {
        //main stories content
        TypedArray ta = resResources.obtainTypedArray(R.array.stories);
        //locations
        // -PRINCESS- May18,18 -- Log.d("CurrentStory: ", " getSpecificLocation = " + Integer.toString(storyNum));
        ta = resResources.obtainTypedArray(R.array.locations);
        int resID = ta.getResourceId(storyNum, 0);
        if (resID > 0) {
            return resResources.getStringArray(resID);
        } else {
            return null;
        }
    }

    public String getSpecificTitle(int storyNum) {
        //main stories content
        TypedArray ta = resResources.obtainTypedArray(R.array.stories);
        //title
        ta = resResources.obtainTypedArray(R.array.titles);
        int resID = ta.getResourceId(storyNum, 0);
        if (resID > 0) {
            return resResources.getStringArray(resID)[iLang];
        } else {
            return null;
        }
    }

    public String getSpecificYear(int storyNum) {
        //main stories content
        TypedArray ta = resResources.obtainTypedArray(R.array.stories);
        //title
        ta = resResources.obtainTypedArray(R.array.years);
        int resID = ta.getResourceId(storyNum, 0);
        if (resID > 0) {
            return resResources.getStringArray(resID)[0];
        } else {
            return null;
        }
    }

    public String getSpecificFeaturedImage(int storyNum) {
        //main stories content
        TypedArray ta = resResources.obtainTypedArray(R.array.stories);
        //images
        ta = resResources.obtainTypedArray(R.array.images);
        int resID = ta.getResourceId(storyNum, 0);
        if (resID > 0) {
            return resResources.getStringArray(resID)[0];
        } else {
            return null;
        }
    }

    public String getSpecificImageCaption(int storyNum) {
        //main stories content
        TypedArray ta = resResources.obtainTypedArray(R.array.stories);
        //images_captions
        ta = resResources.obtainTypedArray(R.array.images_captions);
        int resID = ta.getResourceId(storyNum, 0);
        if (resID > 0) {
            return resResources.getStringArray(resID)[0];
        } else {
            return null;
        }
    }

    public int FindStoryByTitle(String title) {
        String tempStory;
        for (int i = 0; i < iStoryCount; i++) {
            tempStory = getSpecificTitle(i);
            if (tempStory.equals(title)) {
                return i;
            }
        }
        return -1;
    }

    public int FindStoryByYear(String year) {
        String tempStory;
        for (int i = 0; i < iStoryCount; i++) {
            tempStory = getSpecificYear(i);
            if (tempStory.equals(year)) {
                return i;
            }
        }
        return -1;
    }

}
