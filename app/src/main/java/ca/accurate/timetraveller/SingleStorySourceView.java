package ca.accurate.timetraveller;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by P on 2017-01-24.
 */

public class SingleStorySourceView extends FrameLayout implements SourceLinksAdapter.ListItemClickListener  {
    //private LayoutInflater layoutInflater;
    private ViewGroup viewGroup;
    private FragmentManager fragmentManager;
    private Context conContext;
    private View singleStorySourceView;
    private TimelineMainViewPager timelineMainViewPager;
    private MapViewPager mapViewPager;
    private NotificationViewPager notificationViewPager;
    private String link;
    private TextView textSources;
    private int i;

    private SourceLinksAdapter sourceLinksAdapter;
    private RecyclerView recyclerSource;

    public SingleStorySourceView(Context context, ViewGroup container, LayoutInflater inflater, FragmentManager fManager, TimelineMainViewPager tmMainVP, MapViewPager mVPager, NotificationViewPager nViewPager) {
        super(context);
        timelineMainViewPager = tmMainVP;
        mapViewPager = mVPager;
        notificationViewPager = nViewPager;
        conContext = context;
        viewGroup = container;
        //layoutInflater = inflater;
        fragmentManager = fManager;
        LayoutInflater layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        singleStorySourceView = layoutInflater.inflate(R.layout.single_story_source, container, false);

        this.addView(singleStorySourceView);

    }

    public void ResetData() {
        TextView textTitle = (TextView) singleStorySourceView.findViewById(R.id.storyTitle);
        ImageButton backBtn = (ImageButton) singleStorySourceView.findViewById(R.id.back_source);

        String year = CurrentStory.getInstance().getYear();
        String title = CurrentStory.getInstance().getTitle();

        // put year and title with different colors
        SpannableStringBuilder builder = new SpannableStringBuilder();

        // style the year
        SpannableString whiteYear= new SpannableString(year);
        whiteYear.setSpan(new ForegroundColorSpan(Color.WHITE), 0, year.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(whiteYear);

        // style the title
        SpannableString blackTitle = new SpannableString(title);
        blackTitle.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(" " + blackTitle);

        // show title with year in white
        textTitle.setText(builder);


        ArrayList<String> sourceList = new ArrayList<>();
        // get sources
        final String links[] = CurrentStory.getInstance().getLinks();
        link = new String();

        for (i=0; i < links.length; i++) {
            //link += links[i] + "\n\n\n";
            link = links[i];
            sourceList.add(link);
        }


        recyclerSource = (RecyclerView) singleStorySourceView.findViewById(R.id.recycler_source_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerSource.setLayoutManager(layoutManager);

        recyclerSource.setHasFixedSize(true);

        sourceLinksAdapter = new SourceLinksAdapter(sourceList, this);

        recyclerSource.setAdapter(sourceLinksAdapter);

        if(singleStorySourceView.getParent()!=null)
            ((ViewGroup)singleStorySourceView.getParent()).removeView(singleStorySourceView); // <- fix
        viewGroup.addView(singleStorySourceView);
        //container.addView(v);

        backBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapViewPager != null){
                    mapViewPager.setCurrentItem(CurrentStory.getInstance().STORY_VIEW);
                }
                else if(notificationViewPager != null){
                    notificationViewPager.setCurrentItem(0);
                }
                else{
                    timelineMainViewPager.setCurrentItem(CurrentStory.getInstance().STORY_VIEW);
                }

            }
        });
    }

    public void onListItemClick(int clickedItemIndex) {

//        String sourceLink[] = CurrentStory.getInstance().getLinks();
//        String url = sourceLink[clickedItemIndex];
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setData(Uri.parse(url));
//        getContext().startActivity(intent);

    }
    public View GetView() {
        return singleStorySourceView;
    }
}
