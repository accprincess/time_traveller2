
package ca.accurate.timetraveller;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by P on 2017-01-24.
 */

public class MapMainAdapter extends PagerAdapter {
    private SingleStory story;
    private Context conContext;
    private LayoutInflater layoutInflater;
    private FragmentManager fragmentManager;
    private BrowseTimelineView browseView;
    private SingleStoryMapView mapView;
    private SingleStoryDescriptionView descView;
    private SingleStorySourceView sourceView;
    private TimelineMainViewPager tmMainSlider;
    private MapFragmentView mainView;
    private MapViewPager mapViewPager;
    private NotificationViewPager notificationViewPager;
    private int pos;

    public MapMainAdapter(Context con, LayoutInflater inflater, FragmentManager fManager, MapViewPager mVPager, TimelineMainViewPager tVPager) {
        conContext = con;
        tmMainSlider = tVPager;
        layoutInflater = inflater;
        mapViewPager = mVPager;
        fragmentManager = fManager;

        mapViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch(position) {
                    case 0:
                        pos = 0;
                        break;
                    case CurrentStory.STORY_VIEW:
                        if (story != null)
                            story.ResetData();
                        pos = 1;
                        break;
                    case CurrentStory.DESCRIPTION_VIEW:
                        descView.ResetData();
                        pos = 2;
                        break;
                    case CurrentStory.MAP_VIEW:
                        mapView.ResetData();
                        pos = 3;
                        break;
                    case CurrentStory.SOURCE_VIEW:
                        sourceView.ResetData();
                        pos = 4;
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = null;
        switch (position) {
            case 0:
                mainView = new MapFragmentView(conContext, layoutInflater, container, fragmentManager, mapViewPager, tmMainSlider);
                v = mainView.GetView();
                break;
            case 1:
                story = new SingleStory(conContext, container, tmMainSlider, mapViewPager,notificationViewPager );
                story.ResetData();
                v = story.GetView();
                break;
            case 2:
                descView = new SingleStoryDescriptionView(conContext, container, layoutInflater, fragmentManager, tmMainSlider, mapViewPager, notificationViewPager);
                descView.ResetData();
                v = descView.GetView();
                break;
            case 3:
                sourceView = new SingleStorySourceView(conContext, container, layoutInflater, fragmentManager, tmMainSlider, mapViewPager, notificationViewPager);
                sourceView.ResetData();
                v = sourceView.GetView();
                break;
            case 4:
                //v = InitSingleStoryMap(container);
                mapView = new SingleStoryMapView(conContext, container, layoutInflater, fragmentManager, tmMainSlider, mapViewPager, notificationViewPager);
                mapView.ResetData();
                v = mapView.GetView();
                break;
            default:
                //v = InitSingleStoryMap(container);
                break;
        }

        if( v.getParent()!=null)
            ((ViewGroup)v.getParent()).removeView(v); // <- fix
        container.addView(v);

        return v;
    }

    public int getPosition(){ return pos;}

    public void GoToTimelineView() {
        story.GoToTimelineView();
    }

    public void ResetData() {
        story.ResetData();
    }

    public void GoToStoryView() {
        story.GoToStoryView();
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        // remove previous slide
        View v = (View) object;

        //Todo: Image Slider: Improve memory (skip frames error when swiping really fast)
        v.setBackgroundResource(0);

        container.removeView(v);
    }
}
