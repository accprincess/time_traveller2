package ca.accurate.timetraveller;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;

/**
 * Created by P on 2017-01-12.
 */
public class TutorialText {
    private int nStoryNum;
    private int iLang;
    private Context cContext;
    private Resources resResources;
    private String[] sContent;

    private static TutorialText ourInstance = new TutorialText();

    public static TutorialText getInstance() {
        return ourInstance;
    }

    private TutorialText() {
    }

    public void setCurrentTutorial(int storyNum, Context conContext) {
        nStoryNum = storyNum;
        iLang = Language.getInstance().getLanguage();
        cContext = conContext;
        resResources = conContext.getResources();

        //main stories content
        TypedArray ta = resResources.obtainTypedArray(R.array.tutorials);
        int resID = ta.getResourceId(nStoryNum, 0);
        if (resID > 0) {
            sContent = resResources.getStringArray(resID);
        }
    }

    public String getTutIcon(){
         return sContent[2];
    }

    public String getTutTitle() {
        return "";
    }

    public String getTutText() {
        return sContent[iLang];
    }
}
