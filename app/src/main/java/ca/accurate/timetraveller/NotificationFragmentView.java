package ca.accurate.timetraveller;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by Princess on 2017-02-20.
 */

public class NotificationFragmentView extends FrameLayout {

    private Context conContext;
    private LayoutInflater layoutInflater;
    private ViewGroup viewGroup;
    private FragmentManager fragmentManager;
    private NotificationViewPager notificationMainViewPager;
    private View notificationMainView;
    private NotificationMainAdapter nMainAdapter;

    public NotificationFragmentView(Context context, LayoutInflater inflater, ViewGroup container, FragmentManager fManager) {
        super(context);
        conContext = context;
        //layoutInflater = inflater;
        layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewGroup = container;
        fragmentManager = fManager;

        notificationMainView = layoutInflater.inflate(R.layout.fragment_notification_view, container, false);

        notificationMainViewPager = (NotificationViewPager) notificationMainView.findViewById(R.id.notification_main_viewpager);
        nMainAdapter = new NotificationMainAdapter(conContext, layoutInflater, fragmentManager, notificationMainViewPager);
        notificationMainViewPager.setAdapter(nMainAdapter);
        //timelineMainViewPager.setOffscreenPageLimit(4);

        this.addView(notificationMainView);
    }

    public void ResetData() {
        nMainAdapter.ResetData();
    }

    public void GoToTimelineView() {
        nMainAdapter.GoToTimelineView();
    }

    public void GoToStoryView() {
        nMainAdapter.GoToStoryView();
    }

    public int getPosition(){ return nMainAdapter.getPosition();}

    public NotificationViewPager notificationViewPager(){
        return notificationMainViewPager;
    }

}
