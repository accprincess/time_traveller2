package ca.accurate.timetraveller;

/**
 * Created by P on 2016-12-20.
 */
public class Language {

    private int iLang = 0; //0 = default English, 1 = French
    private static Language ourInstance = new Language(0);

    public static Language getInstance() {
        return ourInstance;
    }

    private Language(int lang) {
        iLang = lang;
    }

    public void setLanguage(int lang) {
        iLang = lang;
    }

    public int getLanguage() {
        return iLang;
    }
}
