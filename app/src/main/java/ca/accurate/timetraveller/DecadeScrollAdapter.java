package ca.accurate.timetraveller;


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class DecadeScrollAdapter extends RecyclerView.Adapter<DecadeScrollAdapter.NumberViewHolder> {

    private static final String TAG = DecadeScrollAdapter.class.getSimpleName();
    private ArrayList<String> yearsList;
    final private ListItemClickListener mOnClickListener;
    private String currentYear;
    private int selectedPosition=0;

    public interface ListItemClickListener {
        void onListItemClick(int clickedItemIndex);
    }

    public DecadeScrollAdapter(ArrayList<String> years, ListItemClickListener listener){
        yearsList = years;
        mOnClickListener = listener;

    }
    @Override
    public NumberViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.number_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        NumberViewHolder viewHolder = new NumberViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NumberViewHolder holder, int position) {
        //Log.d(TAG, "#" + position);
        // holder.bind(position);
        holder.listItemNumberView.setText(yearsList.get(position));
        if(selectedPosition ==  position){
            holder.listItemNumberView.setTextColor(Color.parseColor("#8a723b"));
        }
        else {
            holder.listItemNumberView.setTextColor(Color.parseColor("#ffffff"));
        }
    }

    public String GetCurrentYear() {
        return currentYear;
    }


    @Override
    public int getItemCount() {
        return yearsList.size();
    }

    /**
     * Cache of the children views for a list item.
     */
    class NumberViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{

        TextView listItemNumberView;

        public NumberViewHolder(View itemView) {
            super(itemView);

            listItemNumberView = (TextView) itemView.findViewById(R.id.tv_item_number);
            itemView.setOnClickListener(this);
        }

        void bind(int listIndex) {
            listItemNumberView.setText(String.valueOf(listIndex));
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            currentYear = yearsList.get(clickedPosition);
            mOnClickListener.onListItemClick(clickedPosition);
            selectedPosition=clickedPosition;
            notifyDataSetChanged();

        }
    }
}
