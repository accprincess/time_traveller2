package ca.accurate.timetraveller;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by Spencers on 31/01/2017.
 */

public class SettingsMainFragmentView extends FrameLayout {

    private Context conContext;
    private LayoutInflater layoutInflater;
    private ViewGroup viewGroup;
    private FragmentManager fragmentManager;
    private SettingsViewPager settingsMainViewPager;
    private View settingsMainView;
    private SettingsMainAdapter settingsAdapter;

    public SettingsMainFragmentView(Context context, LayoutInflater inflater, ViewGroup container, FragmentManager fManager) {
        super(context);
        conContext = context;
        //layoutInflater = inflater;
        layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewGroup = container;
        fragmentManager = fManager;

        settingsMainView = layoutInflater.inflate(R.layout.fragment_settings_view, container, false);

        settingsMainViewPager = (SettingsViewPager) settingsMainView.findViewById(R.id.settings_main_viewpager);
        settingsAdapter = new SettingsMainAdapter(conContext, layoutInflater, fragmentManager, settingsMainViewPager);
        settingsMainViewPager.setAdapter(settingsAdapter);
        //timelineMainViewPager.setOffscreenPageLimit(4);

        this.addView(settingsMainView);
    }

    public void ResetData() {
        settingsAdapter.ResetData();
    }

    public void GoToTimelineView() {
        settingsAdapter.GoToTimelineView();
    }

    public void GoToStoryView() {
        settingsAdapter.GoToStoryView();
    }

    public int getPosition(){ return settingsAdapter.getPosition();}

    public SettingsViewPager getSettingsMainViewPager(){
        return settingsMainViewPager;
    }
}
