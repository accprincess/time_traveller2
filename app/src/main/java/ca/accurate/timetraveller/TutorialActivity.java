package ca.accurate.timetraveller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Locale;

public class TutorialActivity extends AppCompatActivity {

    // General Variables
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    Button skip;
    ImageButton prev,next;
    private TextView[] dots;
    private LinearLayout dotslayout;
    private int[] layouts;
    private Resources rResources;
    private CurrentStory currStory;
    private Context conContext;
    private ImageView randIcon, mainImgEng, mainImgFr;
    private RelativeLayout randStory;
    private boolean enabledFrench;
    private boolean enabledSwitch;

    private TextView welcome;
    private TextView skipText;
    private LinearLayout imgLogo;
    private LinearLayout imgLogoFr;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_tutorial);

        // Definining variables
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotslayout = (LinearLayout) findViewById(R.id.layoutDots);
        prev = (ImageButton) findViewById(R.id.btn_prev);
        skip = (Button) findViewById(R.id.btn_skip);
        next = (ImageButton) findViewById(R.id.btn_next);
        randIcon = (ImageView) findViewById(R.id.random_icon);
        randStory = (RelativeLayout) findViewById(R.id.random_story);
        welcome = (TextView) findViewById(R.id.tutorial_welcome);
        skipText = (TextView) findViewById(R.id.btn_skip);

        enabledFrench = getSharedPreferences("tutorialSwitch", getBaseContext().MODE_PRIVATE)
                .getBoolean("tutorialSwitch", false);
        enabledSwitch = getSharedPreferences("shared", getBaseContext().MODE_PRIVATE)
                .getBoolean("language", true);

        mainImgEng = (ImageView) findViewById(R.id.tutorialLogo);
        mainImgFr = (ImageView) findViewById(R.id.tutorialLogo_fr);
        Glide.with(this).load(getResources()
                .getIdentifier("main_logo", "drawable", getPackageName()))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(mainImgEng);
        Glide.with(this).load(getResources()
                .getIdentifier("main_logo_fr", "drawable", getPackageName()))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(mainImgFr);

        if(enabledFrench || !enabledSwitch){
            setLocale("fr");
            welcome.setText("Bienvenue!");
            skipText.setText("Continuez");
            imgLogo = (LinearLayout) findViewById(R.id.logoWrapper);
            imgLogoFr = (LinearLayout) findViewById(R.id.logoWrapperFr);
            imgLogo.setVisibility(View.GONE);
            imgLogoFr.setVisibility(View.VISIBLE);
            enabledSwitch = true;
            getBaseContext()
                    .getSharedPreferences("tutorialSwitch", Context.MODE_PRIVATE)
                    .edit()
                    .putBoolean("tutorialSwitch", false)
                    .apply();
        }
        else{
            setLocale("en");
            getBaseContext()
                    .getSharedPreferences("tutorialSwitch", Context.MODE_PRIVATE)
                    .edit()
                    .putBoolean("tutorialSwitch", true)
                    .apply();
        }

        layouts = new int[6];
        for (int i = 0; i < 6; i++) {
            layouts[i] = R.layout.tutorial_layout;
        }
        conContext = this.getBaseContext();
        rResources = getResources();

        addBottomDots(0);
        changeStatusBarColor();
        viewPagerAdapter = new ViewPagerAdapter();
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(viewListener);
        viewPager.setOffscreenPageLimit(2);
        // previous button
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int current = getItem(0);
                Log.d("TutorialActivity", "current: " + current);

                if (current < layouts.length) {
                    viewPager.setCurrentItem(current);
                }
            }
        });

        // skip button
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TutorialActivity.this,MainAppActivity.class);
                startActivity(i);
                randStory.setVisibility(View.VISIBLE);
                randIcon.setVisibility(View.VISIBLE);
                RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                anim.setInterpolator(new LinearInterpolator());
                anim.setRepeatCount(Animation.INFINITE);
                anim.setDuration(2000);
                randIcon.startAnimation(anim);
                finish();
                Log.d("TutorialActivity", "Skip button clicked");
            }
        });

        // next button
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int current = getItem(+1);

                if (current < layouts.length) {
                    viewPager.setCurrentItem(current);
                } else {
//                    Intent i = new Intent(TutorialActivity.this,MainAppActivity.class);
//                    startActivity(i);
//                    finish();
//                    Log.d("TutorialActivity", "Next button clicked");
                }
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    // Adds the bottom dots
    private void addBottomDots(int position) {
        dots = new TextView[layouts.length];
        dotslayout.removeAllViews();
        int color = ContextCompat.getColor(conContext, R.color.dot_dark);
        int activeColor = ContextCompat.getColor(conContext, R.color.dot_light);


        for (int i = 0; i < dots.length; i++) {

            dots[i] = new TextView(this);
            if (Build.VERSION.SDK_INT >= 24) {
                dots[i].setText(Html.fromHtml("&#11044;", Html.FROM_HTML_MODE_LEGACY)); // for 24 api and more
            } else {
                dots[i].setText(Html.fromHtml("&#11044;"));

            }
            dots[i].setTextSize(10);
            dots[i].setPadding(10,30,10,30);
            dots[i].setTextColor(color);
            dotslayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[position].setTextColor(activeColor);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + 1;
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            //refresh all dots
            addBottomDots(position);

            // If it's the last slide, change the button's text
            /*if (position == layouts.length - 1) {
                next.setText("Proceed");
                //skip.setVisibility(View.GONE);
            } else {
                next.setText("Next");
                //skip.setVisibility(View.VISIBLE);
            }*/
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    // Compatibility stuff
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("HomePage Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    public void setLocale(String lang) {

        String langPref = "Language";
        SharedPreferences setPrefs = getBaseContext().getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = setPrefs.edit();
        editor.putBoolean(langPref, false);
        editor.apply();

        Locale myLocale = new Locale(lang);
        Configuration config = new Configuration();
        config.locale = myLocale;
        getResources().updateConfiguration(config, null);

        SharedPreferences prefs = getBaseContext().getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        String lan = prefs.getString("language", "");

    }

    // Slider
    public class ViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = layoutInflater.inflate(R.layout.tutorial_layout, container, false);
            TextView tutorial_text = (TextView) v.findViewById(R.id.tutorial_text);
            TextView tutorial_text2 = (TextView) v.findViewById(R.id.tutorial_text_intro);
            TextView text2 = (TextView) v.findViewById(R.id.text2);
            ImageView ivIcon = (ImageView) v.findViewById(R.id.ivIcon);
            LinearLayout go_btn = (LinearLayout) v.findViewById(R.id.tutorial_go_btn);
            CustomFontButton start_btn = (CustomFontButton) v.findViewById(R.id.tutorial_start);
            RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.background);
            RelativeLayout tRl = (RelativeLayout) v.findViewById(R.id.tutorial_relativeLayout);
            NestedScrollView nested_intro = (NestedScrollView) v.findViewById(R.id.nested_intro);
            currStory.getInstance().setCurrentStory(Integer.toString(position+1), conContext);

            TutorialText.getInstance().setCurrentTutorial(position, conContext);
            /*
            TypedArray ta = rResources.obtainTypedArray(R.array.tutorials);
            int resID = ta.getResourceId(position, 0);
            String[] tut_arr = new String[ta.length()];
            if (resID > 0) {
                tut_arr = rResources.getStringArray(resID);
            }*/
            //Text
            //tutorial_text.setText(tut_arr[Language.getInstance().getLanguage()]);
            tutorial_text.setText(TutorialText.getInstance().getTutText());
            tutorial_text2.setText(TutorialText.getInstance().getTutText());
            //Icon

            int iBackgroundID = getResources().getIdentifier(TutorialText.getInstance().getTutIcon(), "drawable", getPackageName());

            ivIcon.setImageResource(iBackgroundID);
            if (position == 0) {
                tRl.setVisibility(View.GONE);
                tutorial_text2.setVisibility(View.VISIBLE);
            }
            else if(position == 5){
                nested_intro.setVisibility(View.GONE);
                go_btn.setVisibility(View.VISIBLE);
            }


            start_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startBrowsing(view);
                }
            });

            // add next slide
            container.addView(v);
            return v;

        }
        public void startBrowsing(View v) {
            Intent i = new Intent(TutorialActivity.this,MainAppActivity.class);
            startActivity(i);
            randStory.setVisibility(View.VISIBLE);
            randIcon.setVisibility(View.VISIBLE);
            RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setInterpolator(new LinearInterpolator());
            anim.setRepeatCount(Animation.INFINITE);
            anim.setDuration(2000);
            randIcon.startAnimation(anim);
            finish();
        }

        @Override
        public int getCount() {
            //return stories.length;
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

            // remove previous slide
            View v = (View) object;

            //Todo: Image Slider: Improve memory (skip frames error when swiping really fast)
            v.setBackgroundResource(0);

            container.removeView(v);
        }
    }

}
