package ca.accurate.timetraveller;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;

/**
 * Created by P on 2017-01-25.
 */

public class StorySnippetPagerAdapter extends PagerAdapter {
    private Context conContext;
    private LayoutInflater layoutInflater;
    private FragmentManager fragmentManager;
    private TimelineMainViewPager tmMainSlider;
    private ViewGroup viewGroup;
    private int iCount = CurrentStory.getInstance().getStoryCount();

    public StorySnippetPagerAdapter(Context context, LayoutInflater inflater, FragmentManager fManager, TimelineMainViewPager tVPager, ViewGroup container) {
        conContext = context;
        tmMainSlider = tVPager;
        //layoutInflater = inflater;
        viewGroup = container;
        fragmentManager = fManager;
        layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View v = layoutInflater.inflate(R.layout.decade_year_title, container, false);


        Button readStory = (Button) v.findViewById(R.id.read_story);
        ImageView img= (ImageView) v.findViewById(R.id.story_featured_image);

        String featuredImage = CurrentStory.getInstance().getSpecificFeaturedImage(position);
        int img_id = conContext.getResources().getIdentifier(featuredImage, "drawable", conContext.getPackageName());
        //img.setImageResource(img_id);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = 5;
//        img.setImageBitmap(BitmapFactory.decodeResource(conContext.getResources(), img_id, opts));
        Glide.with(conContext).load(img_id)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(img);
        container.setVisibility(View.VISIBLE);

        //text1.setText(CurrentStory.getInstance().getSpecificTitle(position));
        //specYear.setText(CurrentStory.getInstance().getSpecificYear(position));

        //String text = "<font color=#ffffff>"+ CurrentStory.getInstance().getSpecificYear(position) +"</font> <font color=#000000>" + CurrentStory.getInstance().getSpecificTitle(position) + "</font>";
        //text1.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        /*text1.setText("Red text is here", TextView.BufferType.SPANNABLE);
        Spannable span = (Spannable) text1.getText();
        span.setSpan(new ForegroundColorSpan(0xFFFF0000), 0, "Red".length(),
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE);*/
//        Spanned year = Html.fromHtml("<font color='#ffffff'>"+CurrentStory.getInstance().getSpecificYear(position)+ "</font>");
//        Spanned test = Html.fromHtml("<font color='#2bb1ff'>" +CurrentStory.getInstance().getSpecificTitle(position)+"</font>");
//        if (Build.VERSION.SDK_INT >= 24) {
//            text1.setText(test);
//        } else {
//            text1.setText(Html.fromHtml("Multiple style <font color=\"#000000\"><b>Bold with font color</b></font>"));
//        }
        readStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = CurrentStory.getInstance().getSpecificTitle(position);
                int index = CurrentStory.getInstance().FindStoryByTitle(title);
                CurrentStory.getInstance().setCurrentStory(Integer.toString(index+1), conContext);
                tmMainSlider.setCurrentItem(CurrentStory.STORY_VIEW);
            }
        });

        container.addView(v);
        return v;

    }


    @Override
    public int getCount() {
        return CurrentStory.getInstance().getStoryCount();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    //public void destroyItem(ViewGroup container, int position, Object object) {
    public void destroyItem(View collection, int position, Object o) {

//        // remove previous slide
//        View v = (View) object;
//
//        //Todo: Image Slider: Improve memory (skip frames error when swiping really fast)
//        v.setBackgroundResource(0);
//
//        container.removeView(v);
        View view = (View)o;
        ImageView imgView = (ImageView) view.findViewById(R.id.story_featured_image);
//        GlideBitmapDrawable bmpDrawable = (GlideBitmapDrawable) imgView.getDrawable();
//        if (bmpDrawable != null && bmpDrawable.getBitmap() != null) {
//            // This is the important part
//            bmpDrawable.getBitmap().recycle();
//        }
        ((ViewPager) collection).removeView(view);
        view = null;
    }
}
