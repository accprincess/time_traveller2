package ca.accurate.timetraveller;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;

import java.util.ArrayList;

/**
 *
 */
public class MainAppViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<MainAppFragment> fragments = new ArrayList<>();
    private MainAppFragment currentFragment;
    private Context conContext;
    private AHBottomNavigation bottomNav;
    private TimelineMainFragmentView timelineMainFragmentView;

    public MainAppViewPagerAdapter(FragmentManager fm, Context cContext) {
        super(fm);
        conContext = cContext;
        LayoutInflater layoutInflater = (LayoutInflater) conContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fragments.clear();
        fragments.add(MainAppFragment.newInstance(0));
        fragments.add(MainAppFragment.newInstance(1));
        fragments.add(MainAppFragment.newInstance(2));
        fragments.add(MainAppFragment.newInstance(3));
        fragments.add(MainAppFragment.newInstance(4));
    }

    public void SetBottomNav(AHBottomNavigation bottomNavigation) {
        bottomNav = bottomNavigation;
        MainAppFragment randomFragment = fragments.get(2);
        randomFragment.SetBottomNav(bottomNavigation);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            currentFragment = ((MainAppFragment) object);
        }
        super.setPrimaryItem(container, position, object);
    }

    public void setCurrentItem(int item) {
        currentFragment = fragments.get(item);
    }

    /**
     * Get the current fragment
     */
    public MainAppFragment getCurrentFragment() {
        return currentFragment;
    }

    public MainAppFragment getMapF(){
        return currentFragment = fragments.set(1, currentFragment);
    }
}