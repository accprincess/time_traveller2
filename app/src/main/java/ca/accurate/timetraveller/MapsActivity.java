package ca.accurate.timetraveller;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
//import com.google.android.gms.location.LocationListener;
import android.location.LocationListener;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.MapStyleOptions;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, com.google.android.gms.location.LocationListener, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap gMap;
    private Location locLastLocation;
    public LocationManager locLocationManager;
    private GoogleApiClient mGoogleApiClient;
    private Marker[] marMarkers;
    private Bitmap[] bitBitmapDefaults, bitBitmapHighlights;
    private LocationRequest locLocationRequest;
    private Marker marCurrLocationMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //Need to check SDK versions and permissions to Access User's Location/GPS
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //3 tabs bottom navigation
        AHBottomNavigation bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

        // Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.map_icon, R.color.color_tab_1);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.timeline_icon, R.color.color_tab_2);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.random_icon, R.color.color_tab_3);

        // Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);

        // Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));

        // Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(false);

        // Enable the translation of the FloatingActionButton
        //bottomNavigation.manageFloatingActionButtonBehavior(floatingActionButton);

        // Change colors
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

        // Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

        // Display color under navigation bar (API 21+)
        // Don't forget these lines in your style-v21
        // <item name="android:windowTranslucentNavigation">true</item>
        // <item name="android:fitsSystemWindows">true</item>
        bottomNavigation.setTranslucentNavigationEnabled(true);

        // Manage titles
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);

        // Use colored navigation with circle reveal effect
        bottomNavigation.setColored(true);

        // Set current item programmatically
        bottomNavigation.setCurrentItem(1);

        // Customize notification (title, background, typeface)
        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));

        // Add or remove notification for each item
        //        bottomNavigation.setNotification("1", 3);
        // OR
        /*AHNotification notification = new AHNotification.Builder()
                .setText("1")
                .setBackgroundColor(ContextCompat.getColor(MapsActivity.this, R.color.color_notification_back))
                .setTextColor(ContextCompat.getColor(MapsActivity.this, R.color.color_notification_text))
                .build();
        bottomNavigation.setNotification(notification, 1);*/

        // Set listeners
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                // Do something cool here...
                Intent i;
                //May18,18 - Princess removed
//                if (position > 1) {
//                    i = new Intent(MapsActivity.this, TestTabbedActivity.class);
//                } else {
//                    i = new Intent(MapsActivity.this, SingleStoryActivity.class);
//                }
                //startActivity(i);
                finish();
                return true;
            }
        });
        bottomNavigation.setOnNavigationPositionListener(new AHBottomNavigation.OnNavigationPositionListener() {
            @Override public void onPositionChange(int y) {
                // Manage the new y position
            }
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;

        //get the number of stories
        int iCount = CurrentStory.getInstance().getStoryCount();

        //setup, keep all markers and bitmaps in array so we don't need to recreate any new data
        marMarkers = new Marker[iCount];
        bitBitmapDefaults = new Bitmap[iCount];
        bitBitmapHighlights = new Bitmap[iCount];

        //Initialize Google Play Services
        // Uncomment back - Princess 07/24

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                gMap.setMyLocationEnabled(true);

            }
        }
        else {
            buildGoogleApiClient();
            gMap.setMyLocationEnabled(true);

        }
        // Uncomment back - Princess 07/24

        //loop through each story and put its location pin on the map
        for(int i = 0; i < iCount; i++) {
            //get lat/long
            String loc[] = CurrentStory.getInstance().getSpecificLocation(i);
            if (loc != null) {
                //convert lat/long string to float
                LatLng llStory = new LatLng(Float.parseFloat(loc[0]), Float.parseFloat(loc[1]));

                //create default and highlight bitmaps for icons
                Bitmap bMapIconDefault = createMapIcon(CurrentStory.getInstance().getSpecificYear(i), Color.rgb(188, 155, 81), true);
                Bitmap bMapIconHighlight = createMapIcon(CurrentStory.getInstance().getSpecificYear(i), Color.rgb(255, 255, 255), false);

                //create/add marker to gmap
                Marker mk = gMap.addMarker(new MarkerOptions().position(llStory).title(CurrentStory.getInstance().getSpecificTitle(i)).snippet(CurrentStory.getInstance().getSpecificYear(i)).icon(BitmapDescriptorFactory.fromBitmap(bMapIconDefault)) );

                //save marker and bitmaps into their arrays
                marMarkers[i] = mk;
                bitBitmapDefaults[i] = bMapIconDefault;
                bitBitmapHighlights[i] = bMapIconHighlight;



                //try styling the gmaps (as there could be issues on some version of Android
                try {
                    // Customise the styling of the base map using a JSON object defined
                    // in a raw resource file.
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    this, R.raw.style_json));

                    if (!success) {
                        Log.e(TAG, "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e(TAG, "Can't find style. Error: ", e);
                }


            }
        }
        //initialize camera position to first marker that was added
        gMap.moveCamera(CameraUpdateFactory.newLatLng(marMarkers[0].getPosition()));
        //set default zoom level
        gMap.moveCamera(CameraUpdateFactory.zoomTo(12));

        //add click listeners
        gMap.setOnMarkerClickListener(this);
        gMap.setOnMapClickListener(this);
        //gMap.setOnMyLocationButtonClickListener(this);

    }

    //I put this in a function since it's used to create both highlight and default icons. could be moved or
    //repurposed into a new class or function if needed. Perhaps create a singleton class to hold all the bitmaps?
    //we'll see how it affects performance
    private Bitmap createMapIcon(String year, int color, Boolean isDefault) {
        Bitmap icon;
        //get the png graphic based on if we want the default or highlighted icon states
        if (isDefault)
            icon = BitmapFactory.decodeResource(getResources(), R.drawable.map_icon_default);
        else
            icon = BitmapFactory.decodeResource(getResources(), R.drawable.map_icon_white);

        //create bitmap from the png
        Bitmap bMapScaled = Bitmap.createScaledBitmap(icon, 166, 200, true);
        //create canvas so we can add text
        Canvas canvas = new Canvas(bMapScaled);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //color from user
        paint.setColor(color);

        // text size in pixels
        float scale = Resources.getSystem().getDisplayMetrics().density;
        paint.setTextSize((int) (2 * scale));

        // draw text to the top of the Canvas
        Rect bounds = new Rect();
        paint.getTextBounds(year, 0, year.length(), bounds);
        //set font
        paint.setTypeface(CustomFont.getInstance().AbrilFont(getBaseContext(), "abril" ));
        //set position (center top)
        int x = (bMapScaled.getWidth() - bounds.width())/2;
        int y = bounds.height();
        //draw text on icon
        canvas.drawText(year, x, y, paint);

        //return finished bitmap icon with png and year text
        return bMapScaled;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.setAlwaysShow(true);
    }

    @Override
    public void onLocationChanged(Location location) {

        locLastLocation = location;
        if (marCurrLocationMarker != null) {
            marCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        /*
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        marCurrLocationMarker = gMap.addMarker(markerOptions);
        */
        //move map camera
        gMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        gMap.animateCamera(CameraUpdateFactory.zoomTo(12));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }


    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        int index = -1;
        for (int i = 0; i < marMarkers.length; i++)
        {
            //find the marker that was clicked
            if (marMarkers[i].getTitle().equals(marker.getTitle()) ) {
                index = i;
            }

            //reset all markers to default icon
            marMarkers[i].setIcon(BitmapDescriptorFactory.fromBitmap(bitBitmapDefaults[i]));
        }

        //set icon of clicked marker to highlight
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitBitmapHighlights[index]));

        //hide info window (although it doesn't seem to work. we need to figure out how to hide
        //and instead will be showing a popup at the bottom of the screen
        marker.hideInfoWindow();

        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locLocationRequest = new LocationRequest();
        locLocationRequest.setInterval(1000);
        locLocationRequest.setFastestInterval(1000);
        locLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "Connection suspended, please unsuspend.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "GPS not enabled, please enable.", Toast.LENGTH_LONG).show();
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Permission was granted.
                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }

                        gMap.setMyLocationEnabled(true);
                        Toast.makeText(this, "Location found.", Toast.LENGTH_LONG).show();
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            //You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        //reset all icons to default if map area is clicked.
        //and we'll have to hide the bottom popup once we create it
        for (int i = 0; i < marMarkers.length; i++)
        {
            //reset all markers to default icon
            marMarkers[i].setIcon(BitmapDescriptorFactory.fromBitmap(bitBitmapDefaults[i]));
        }
    }
    /*
    @Override
    public boolean onMyLocationButtonClick() {
        gMap.moveCamera(CameraUpdateFactory.newLatLng(marCurrLocationMarker.getPosition()));
        return false;
    }*/
}
