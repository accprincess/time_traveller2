package ca.accurate.timetraveller;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class LoadingScreenActivity extends AppCompatActivity {
    //constant variable for Splash screen display lenght (in milliseconds)
    private final int SPLASH_DISPLAY_LENGTH = 2500;
    private String sLang;
    private SharedPreferences spPref;
    private Intent mIntentService;

    //    private SensorService mSensorService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //remove title bar, also in the manifest.xml
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_loading_screen);

        ImageView imageView = (ImageView) findViewById(R.id.imageView8);
        Glide.with(this).load(getResources()
                .getIdentifier("main_logo", "drawable", getPackageName()))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView);

        VideoView mVideoView = (VideoView) findViewById(R.id.videoView);
        mVideoView.setVideoPath("android.resource://"+getPackageName()+"/raw/main_1000x1000px");

        mVideoView.setMediaController(new MediaController(this));
        mVideoView.seekTo(1);
        mVideoView.setVisibility(View.VISIBLE);
        mVideoView.start();


        mIntentService = new Intent(this, LocationCheckerService.class);
        startService(mIntentService);

        /*AssetManager am = this.getApplicationContext().getAssets();
        Typeface typeface;
        typeface = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "AbrillFatface-Regular.ttf"));

        setTypeface(typeface);*/


        new CountDownTimer(SPLASH_DISPLAY_LENGTH, 500) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                showDialog();
            }
        }.start();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

    @Override
    protected void onDestroy() {
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();

        stopService(mIntentService);
    }

    private void showDialog() {
        // custom dialog
        final Dialog diaDialog = new Dialog(this);
        // Include dialog.xml file
        diaDialog.setContentView(R.layout.custom_dialog_main_layout);

        //get dialog title textView
        TextView tvDialogTitle = (TextView) diaDialog.findViewById(R.id.textDialog);
        tvDialogTitle.setText("Set language.");
        //Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/AbrilFatface-Regular.ttf");
        tvDialogTitle.setTypeface( CustomFont.getInstance().AbrilFont(this, "spin") );
        tvDialogTitle.setGravity(Gravity.CENTER);

        //find buttons and set click listeners further below
        Button btnEnglish = (Button) diaDialog.findViewById(R.id.EnglishButton);
        Button btnFrench = (Button) diaDialog.findViewById(R.id.FrenchButton);
        btnEnglish.setTypeface( CustomFont.getInstance().AbrilFont(this, "abril") );
        btnFrench.setTypeface( CustomFont.getInstance().AbrilFont(this, null ) );

        //get sharedPreferences (saved info such as language)
        spPref = getSharedPreferences( getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        sLang = spPref.getString( getString(R.string.lang), null );
        Log.d("LoadingScreenActivity", "lang = " + sLang);

        //debug text in dialog
        TextView tvDebugText = (TextView) diaDialog.findViewById(R.id.debugData);
        tvDebugText.setText("lang = " + sLang);

        if (sLang != null) {
            diaDialog.show();
        } else {
            Intent iIntent = new Intent(LoadingScreenActivity.this, TutorialActivity.class);
            startActivity(iIntent);
        }

        // if French button is clicked, set language to French and go to home Page
        btnFrench.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //save language choice in a sharedpreference file
                SharedPreferences.Editor spEditor = spPref.edit();
                spEditor.putString("lang", "fr");
                spEditor.commit();
                Language.getInstance().setLanguage(1);
                //start home page activity
                Intent iIntent = new Intent(LoadingScreenActivity.this, TutorialActivity.class);
                startActivity(iIntent);
                // Close dialog
                diaDialog.dismiss();
            }
        });


        // if English button is clicked, set language to English and go to Home page
        btnEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //save language choice in a sharedpreference file
                SharedPreferences.Editor spEditor = spPref.edit();
                spEditor.putString("lang", "en");
                spEditor.commit();
                Language.getInstance().setLanguage(0);
                //start home page activity
                Intent iIntent = new Intent(LoadingScreenActivity.this, TutorialActivity.class);
                startActivity(iIntent);
                // Close dialog
                diaDialog.dismiss();
            }
        });
    }

    // Launching the service
    public void onStartService() {
        Intent i = new Intent(this, MainAppService.class);
        i.putExtra("foo", "bar");
        startService(i);

    }

    @Override
    protected void onResume() {
        super.onResume();
        // Register for the particular broadcast based on ACTION string
        IntentFilter filter = new IntentFilter(MainAppService.ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(testReceiver, filter);
        // or `registerReceiver(testReceiver, filter)` for a normal broadcast
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(this).unregisterReceiver(testReceiver);
        // or `unregisterReceiver(testReceiver)` for a normal broadcast
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int resultCode = intent.getIntExtra("resultCode", RESULT_CANCELED);
            if (resultCode == RESULT_OK) {
                String resultValue = intent.getStringExtra("resultValue");
                Toast.makeText(LoadingScreenActivity.this, resultValue, Toast.LENGTH_SHORT).show();
            }
        }
    };

}

