package ca.accurate.timetraveller;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by P on 2016-12-19.
 */
public class CustomFont extends AppCompatActivity {

    private static Typeface abril_font;
    private static Typeface spin_font;

    private static CustomFont ourInstance = new CustomFont();

    public static CustomFont getInstance() {
        return ourInstance;
    }

    Typeface AbrilFont(Context context, String sFont) {
        abril_font = Typeface.createFromAsset(context.getAssets(),  "fonts/AbrilFatface-Regular.ttf");
        spin_font = Typeface.createFromAsset(context.getAssets(),  "fonts/Spinnaker-Regular.ttf");

        if (sFont == "abril") {
            return abril_font;
        } else if (sFont == "spin") {
            return spin_font;
        } else {
            return spin_font;
        }
    }
}
