package ca.accurate.timetraveller;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by P on 2017-01-30.
 */

public class SettingsMainAdapter extends PagerAdapter {

    private Context conContext;
    private LayoutInflater layoutInflater;
    private FragmentManager fragmentManager;
    private SettingsViewPager settingsSlider;
    private SettingsAboutView aboutView;
    private MainSettingsActivity mainView;
    private SettingsHelpView helpView;
    private int pos;

    public SettingsMainAdapter(Context con, LayoutInflater inflater, FragmentManager fManager, SettingsViewPager settingsViewPager) {
        conContext = con;
        layoutInflater = inflater;
        fragmentManager = fManager;
        settingsSlider = settingsViewPager;

        settingsSlider.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch(position) {
                    case 0:
                        pos = 0;
                        break;
                    case 1:
                        pos = 1;
                        break;
                    case 2:
                        pos = 2;
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = null;
        switch (position) {
            case 0:
                mainView = new MainSettingsActivity(conContext, layoutInflater, container, fragmentManager, settingsSlider);
                v = mainView.GetView();
                break;
            case 1:
                helpView = new SettingsHelpView(conContext, container, layoutInflater, fragmentManager, settingsSlider);
                helpView.ResetData();
                v = helpView.GetView();
                break;
            case 2:
                aboutView = new SettingsAboutView(conContext, container, layoutInflater, fragmentManager, settingsSlider);
                aboutView.ResetData();
                v = aboutView.GetView();
                break;
            default:
                //v = InitSingleStoryMap(container);
                break;
        }

        if( v.getParent()!=null)
            ((ViewGroup)v.getParent()).removeView(v); // <- fix
        container.addView(v);

        return v;
    }

//    public void GoToTimelineView() {
//        story.GoToTimelineView();
//    }
//
//    public void ResetData() {
//        story.ResetData();
//    }
//
//    public void GoToStoryView() {
//        story.GoToStoryView();
//    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        // remove previous slide
        View v = (View) object;

        //Todo: Image Slider: Improve memory (skip frames error when swiping really fast)
        v.setBackgroundResource(0);

        container.removeView(v);
    }

    public void GoToTimelineView() {
        //settingsSlider.GoToTimelineView();
    }

    public void ResetData() {
        //settingsSlider.ResetData();
    }

    public void GoToStoryView() {
        //settingsSlider.GoToStoryView();
    }

    public int getPosition(){
        return pos;
    }
}
