package ca.accurate.timetraveller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;


/**
 *
 */
public class MainAppFragment extends Fragment {// implements OnMapReadyCallback, com.google.android.gms.location.LocationListener, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private FrameLayout fragmentContainer;
    private RecyclerView recyclerView;
    private int fragmentIndex;
    private MapMainFragmentView mapMainFragmentView;
    private TimelineMainFragmentView timelineMainFragmentView;
    private SettingsMainFragmentView settingsView;
    private NotificationFragmentView nFragmentView;
    private RandomFragmentView randomView;
    private AHBottomNavigation bottomNav;

    /**
     * Create a new instance of the fragment
     */
    public static MainAppFragment newInstance(int index) {
        MainAppFragment fragment = new MainAppFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        switch(getArguments().getInt("index", 0)) {
            case 0:
                mapMainFragmentView = new MapMainFragmentView(getContext(), inflater, container, this.getChildFragmentManager());
                fragmentIndex = 0;

                return mapMainFragmentView;
            case 1:
                timelineMainFragmentView = new TimelineMainFragmentView(getContext(), inflater, container, this.getChildFragmentManager());
                fragmentIndex = 1;
                return timelineMainFragmentView;
            case 2:
                randomView = new RandomFragmentView(getContext(), inflater, container, this.getChildFragmentManager());
                fragmentIndex = 2;
                return randomView;
            case 3:
                settingsView = new SettingsMainFragmentView(getContext(), inflater, container, this.getChildFragmentManager());
                fragmentIndex = 3;
                return settingsView;
            case 4:
                nFragmentView = new NotificationFragmentView(getContext(), inflater, container, this.getChildFragmentManager());
                fragmentIndex = 4;
                return nFragmentView;
            default:
                return null;
        }
    }


    public int getMapViewPosition(){
        return mapMainFragmentView.getPosition();
    }

    public int getTimelineViewPosition(){
        return timelineMainFragmentView.getPosition();
    }
    public int getNotificationPosition(){
        return nFragmentView.getPosition();
    }
    public int getSettingsViewPosition(){
        return settingsView.getPosition();
    }

    public TimelineMainViewPager getTimelineViewPager(){
        return timelineMainFragmentView.timelineMainViewPager();
    }

    public MapViewPager getMapViewPager(){
        return mapMainFragmentView.getMapMainViewPager();
    }

    public SettingsViewPager getSettingsViewPager(){
        return settingsView.getSettingsMainViewPager();
    }
    public NotificationViewPager getNotificationViewPager(){
        return nFragmentView.notificationViewPager();
    }

    public int getIndex() {
        return  fragmentIndex;
    }

    /**
     * Refresh
     */
    public void refresh(int index) {
        if (getArguments().getInt("index", 0) > 0 && recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
        if (index == 1) {
            timelineMainFragmentView.ResetData();
            timelineMainFragmentView.GoToStoryView();
        }
        else if (index == 2) {
            randomView.SetBottomNav(bottomNav);
            if (randomView != null)
                randomView.refresh();

        }
    }

    public void SetBottomNav(AHBottomNavigation bot) {
        bottomNav = bot;
    }

    /**
     * Called when a fragment will be displayed
     */
    public void willBeDisplayed() {
        // Do what you want here, for example animate the content
        if (fragmentContainer != null) {
            Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            fragmentContainer.startAnimation(fadeIn);
        }
    }

    /**
     * Called when a fragment will be hidden
     */
    public void willBeHidden() {
        if (fragmentContainer != null) {
            Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
            fragmentContainer.startAnimation(fadeOut);
        }
    }


}
