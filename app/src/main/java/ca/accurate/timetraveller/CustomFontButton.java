package ca.accurate.timetraveller;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

public class CustomFontButton extends android.support.v7.widget.AppCompatButton {

    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";


    public CustomFontButton(Context context) {
        super(context);
    }

    public CustomFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context, attrs);
    }

    public CustomFontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context, attrs);
    }


    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.CustomFontButton);

        String fontName = attributeArray.getString(R.styleable.CustomFontButton_font);

        Typeface customFont = selectTypeface(context, fontName);

        setTypeface(customFont);

        attributeArray.recycle();
    }

    private Typeface selectTypeface(Context context, String fontName) {
        if (fontName.contentEquals(context.getString(R.string.font_abril))) {
            //return Typeface.createFromAsset(context.getAssets(), "fonts/AbrilFatface-Regular.ttf");
            return CustomFont.getInstance().AbrilFont(context, fontName);
        }
        else if (fontName.contentEquals(context.getString(R.string.font_spinnaker))) {
            //return Typeface.createFromAsset(context.getAssets(), "fonts/Spinnaker-Regular.ttf");
            return CustomFont.getInstance().AbrilFont(context, fontName);
        }
        else {
            // no matching font found
            // return null so Android just uses the standard font (Roboto)
            return null;
        }
    }

}
