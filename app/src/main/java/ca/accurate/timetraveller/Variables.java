package ca.accurate.timetraveller;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;

/**
 * Created by Spencers on 2/13/2017.
 */

public class Variables {
    //runnable delay when random icon clicked, it's here so it can be disabled/stopped when another icon is clicked on
    public Runnable randomDelay;

    //TimelineFragmentView ViewPager fragments
    public static final int TIMELINE_VIEW = 0;
    public static final int STORY_VIEW = 1;
    public static final int DESCRIPTION_VIEW = 2;
    public static final int SOURCE_VIEW = 3;
    public static final int MAP_VIEW = 4;

    //Current MainAppViewPager fragment. Needed to do things to current fragment/view
    public MainAppFragment currentFragment;

    //TimelineMainViewPager, needed to switch between timeline viewpager views
    public TimelineMainViewPager timelineMainViewPager;

    //MainAppActivity, needed for Context and other things
    public MainAppActivity mainAppActivity;

    //Bottom Navigation needed to refresh and force click icons at the bottom
    public AHBottomNavigation bottomNavigation;

    //Bottom navigation buttons
    public static final int CLICKED_FROM_MAP_ICON = 0;
    public static final int CLICKED_FROM_TIMELINE_ICON = 1;
    public static final int CLICKED_FROM_RANDOM_ICON = 2;
    public static final int CLICKED_FROM_SETTINGS_ICON = 3;

    private static Variables ourInstance = new Variables();

    public static Variables getInstance() {
        return ourInstance;
    }

    private Variables() {

    }

}
