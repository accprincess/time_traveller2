package ca.accurate.timetraveller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainAppNavigation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_app_nav_layout);
    }
}
